function on_page_load_pagination(){
    if (pagination_page > 5){
        let left_pagination = 2;
        let right_pagination = 2;
        
        if ((current_page - left_pagination < 1) && (current_page + right_pagination <= pagination_page)){
            if (current_page - left_pagination === 0) {
                left_pagination = left_pagination - 1
                right_pagination = right_pagination + 1
            }
            else if (current_page - left_pagination === -1) {
                left_pagination = left_pagination - 2
                right_pagination = right_pagination + 2
                document.getElementById('btn__previous__pagination').style.display = 'none'
                document.getElementById('btn__first__pagination__page').style.display = 'none'
            }
        }
        else if ((current_page - left_pagination >= 1) && (current_page + right_pagination > pagination_page)){
            if (current_page + right_pagination === pagination_page + 1){
                left_pagination = left_pagination + 1
                right_pagination = right_pagination -1
            }
            else if (current_page + right_pagination === pagination_page + 2){
                left_pagination = left_pagination + 2
                right_pagination = right_pagination -2
                document.getElementById('btn__next__pagination').style.display = 'none'
                document.getElementById('btn__end__pagination__page').style.display = 'none'
            }
        }
        let all_btn_pagination = document.getElementsByClassName('btn__pagination');
        
        for(let index = 0; index < all_btn_pagination.length; index++){
            all_btn_pagination[index].style.display = 'none'
        }

        document.getElementById(`btn__page__pagination__page${current_page}`).style.display = 'inline-block';
        
        for (let index = 0; index < left_pagination; index ++){
            document.getElementById(`btn__page__pagination__page${current_page - (index + 1)}`).style.display = 'inline-block';
        }
        for (let index = 0; index < right_pagination; index ++){
            document.getElementById(`btn__page__pagination__page${current_page + (index + 1)}`).style.display = 'inline-block';
        }
    }
}

function pre_btn_pagination(){
    current_page = current_page - 1;
    document.getElementById(`btn__page__pagination__page${current_page}`).click();
}

function next_btn_pagination(){
    current_page = current_page + 1;
    document.getElementById(`btn__page__pagination__page${current_page}`).click();
}

function first_page_pagination(){
    current_page = 1;
    document.getElementById(`btn__page__pagination__page${current_page}`).click();
}

function last_page_pagination(){
    current_page = pagination_page;
    document.getElementById(`btn__page__pagination__page${current_page}`).click();
}