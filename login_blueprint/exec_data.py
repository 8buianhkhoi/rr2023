from sqlalchemy import select, and_

from models.models import users, engine

def check_login(username_login, password_login):
    try:
        with engine.connect() as conn:
            query_login = select(users.c.full_name, users.c.user_name, users.c.type_user, users.c.id).where(and_(users.c.user_name == username_login, users.c.password == password_login))
            get_login = conn.execute(query_login).rowcount

            if get_login == 0:
                result_login = []
            elif get_login == 1:
                result_login = conn.execute(query_login).fetchall()
        return result_login
    except:
        return False