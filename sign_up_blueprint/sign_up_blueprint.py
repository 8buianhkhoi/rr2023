from flask import Blueprint, render_template, request
from dotenv import load_dotenv
import hashlib
import base64
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from . import exec_data

sign_up_blueprint = Blueprint('SU_bp', __name__, static_folder = 'static', template_folder = 'templates')

def hash_pass(password, salt = None, iterations = 100000, key_length = 32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen = key_length)
    return salt + key 

@sign_up_blueprint.route('/verify-account-success/<gmail_verify>')
def sign_up_verify_gmail_success(gmail_verify):
    return_para = {}

    update_verify_gmail = exec_data.update_verify_gmail_status(gmail_verify)

    if update_verify_gmail == True:
        return_para['update_verify_gmail_success'] = 'Xác thực gmail thành công'
    else:
        return_para['update_verify_gmail_fail'] = 'Xác thực gmail thất bại'

    return render_template('verify_gmail_success.html', **return_para)

@sign_up_blueprint.route('/', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        # Because each user have different username, name, tel and gmail
        # So we need to check it, if it exits in database throw error
        # I use rowcount property to count the row if where statement
        # if return 0 it mean not exits

        # Name display and username is different. username just for login
        # Create 1 dictionary to store error duplicate
        get_username = request.form['username_signup']
        get_tel = request.form['tel_signup']
        get_gmail = request.form['gmail_signup']
        get_name = request.form['name_signup']
        get_password = request.form['password_signup']
        get_gender = request.form['gender_signup']

        load_dotenv()
        secret_key = os.getenv('secret_key')

        hash_bytes = hash_pass(get_password, secret_key.encode())
        get_password = base64.b64encode(hash_bytes).decode('utf-8')

        check_duplicate = exec_data.check_duplicate_signup(username_para = get_username, tel_para = get_tel, gmail_para = get_gmail, name_para = get_name)
        if check_duplicate != True:
            return render_template('sign_up.html', err_dict = check_duplicate)

        ins_new_user = exec_data.ins_new_user(username_para = get_username, name_para = get_name, pass_para = get_password,
            gmail_para = get_gmail, tel_para = get_tel, gender_para = get_gender)
        if ins_new_user == True:
            message = Mail(from_email = '9buianhkhoi@gmail.com', to_emails = get_gmail, subject='Test sending gmail verify',
            html_content=f'<strong>Click <a href="http://127.0.0.1:5000/sign-up/verify-account-success/{get_gmail}">here</a> To verify account !</strong>')
            
            sg = SendGridAPIClient(os.getenv('send_grid_api_key'))
            response = sg.send(message)

            return render_template('sign_up.html', ins_success = ins_new_user)
        elif ins_new_user == False:
            return render_template('sign_up.html', ins_fail = ins_new_user)

    return render_template('sign_up.html')