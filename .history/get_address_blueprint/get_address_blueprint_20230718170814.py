from flask import Blueprint, jsonify, request
from sqlalchemy import and_, select

from models.models import division, engine

get_address_blueprint = Blueprint('GA_bp', __name__)

@get_address_blueprint.route('/get-province', methods = ['POST'])
def get_all_province():
    with engine.connect() as conn:
        query_get_province = select(division.c.province.distinct()).order_by(division.c.province)
        get_all_province = conn.execute(query_get_province).fetchall()
        get_all_province = [tuple(row) for row in get_all_province]

    return jsonify(result = get_all_province)
    
@get_address_blueprint.route('/get-district', methods = ['POST'])
def get_all_district():
    province = request.form['province']

    with engine.connect() as conn:
        query_district = select(division.c.district.distinct()).where(division.c.province == province).order_by(division.c.district)
        get_districts = conn.execute(query_district).fetchall()
        get_districts = [tuple(row) for row in get_districts]

    return jsonify(result = get_districts)

@get_address_blueprint.route('/get-wards', methods = ['POST'])
def get_all_ward():
    with engine.connect() as conn:
        province = request.form['province']
        district = request.form['district']
        query_all_ward = select(division.c.ward.distinct()).where(and_(division.c.province == province, division.c.district == district))
        all_wards = conn.execute(query_all_ward).fetchall()
        all_wards = [tuple(row) for row in all_wards]

    return jsonify(result = all_wards)