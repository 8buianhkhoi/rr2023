# Import library
from flask import Flask
import firebase_admin
from firebase_admin import credentials
import platform

# Import homepage blueprint
from homepage_blueprint.homepage_blueprint import homepage_blueprint

# Import login blueprint
from login_blueprint.login_blueprint import login_blueprint

# Import sign up blueprint
from sign_up_blueprint.sign_up_blueprint import sign_up_blueprint

# Import user blueprint
from user_blueprint.user_blueprint import user_blueprint

# Import editor blueprint
from editor_blueprint.editor_blueprint import editor_blueprint

# Import admin blueprint
from admin_blueprint.admin_blueprint import admin_blueprint

# Import get address blueprint
# Get address blueprint help load province, district and ward
from get_address_blueprint.get_address_blueprint import get_address_blueprint

# Import generate pdf blueprint
from generate_pdf_blueprint.generate_pdf_blueprint import generate_pdf_blueprint

# Create flask app and config secret key. In the future we need to edit secret key with key in .env file , key is secret_app
# Need edit
app = Flask(__name__)
app.config['SECRET_KEY'] = 'DEV'

# add homepage blueprint
app.register_blueprint(homepage_blueprint, url_prefix = '/homepage')

# add login blueprint
app.register_blueprint(login_blueprint, url_prefix = '/login')

# add sign up blueprint
app.register_blueprint(sign_up_blueprint, url_prefix = '/sign-up')

# add user blueprint
app.register_blueprint(user_blueprint, url_prefix = '/user')

# add editor blueprint
app.register_blueprint(editor_blueprint, url_prefix ='/editor')

# add get address blueprint
app.register_blueprint(get_address_blueprint, url_prefix = '/get-address')

# add admin blueprint
app.register_blueprint(admin_blueprint, url_prefix = '/admin-control')

# add generate pdf blueprint
app.register_blueprint()

cred = credentials.Certificate('static/json/serviceAccountKey.json')

firebase_admin.initialize_app(cred, {
    'databaseURL':'https://rr2023-110aa-default-rtdb.firebaseio.com/',
    'storageBucket': 'rr2023-110aa.appspot.com'
})

if __name__ == "__main__":
    # This config help template auto reload, don't need reload server
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True

    # run web
    if platform.system() == 'Linux':
        app.run(host='0.0.0.0')
    elif platform.system() == 'Windows':
       app.run(debug = True,  use_reloader = False)