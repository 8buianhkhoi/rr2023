from flask import Blueprint

generate_xlsx_blueprint = Blueprint('GEN_XLSX_bp', __name__, static_folder = 'static', template_folder = 'templates')