from flask import Blueprint, send_file, session, request, render_template
import xlsxwriter
import datetime

from models.models import *
from . import exec_data

generate_xlsx_blueprint = Blueprint('GEN_XLSX_bp', __name__, static_folder = 'static', template_folder = 'templates')

@generate_xlsx_blueprint.route('/generate/<int:current_page_url>')
def generate_xlsx(current_page_url = 1):
    return_para = {}

    exec_all_booking_room = exec_data.get_all_booking_room(current_page_url)
    if exec_all_booking_room != False:
        return_para['get_all_booking'] = exec_all_booking_room['get_all_booking']
        return_para['pagination_page'] = exec_all_booking_room['pagination_page']
    
    return_para['current_page'] = current_page_url

    return render_template('generate_xlsx_view.html', **return_para)

@generate_xlsx_blueprint.route('/get-excel-all')
def download_xlsx_all():
    workbook = xlsxwriter.Workbook('demo.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write(0,0,'id booking room')
    worksheet.write(0,1,'id users')
    worksheet.write(0,2,'id all room')
    worksheet.write(0,3,'time booking')
    worksheet.write(0,4,'time start')
    worksheet.write(0,5,'default service')
    worksheet.write(0,6,'option service')
    worksheet.write(0,7,'minute rent')
    worksheet.write(0,8,'status')
    worksheet.write(0,9,'total price')
    worksheet.write(0,10,'option service price')
    worksheet.write(0,11,'note booking')
    worksheet.write(0,12,'code booking')
    worksheet.write(0,13,'billing option')
    worksheet.write(0,14,'flag booking')

    row = 1

    exec_all_booking_room = exec_data.get_all_booking_room_without_pagination()

    if exec_all_booking_room != False:
        get_all_booking_room = exec_all_booking_room['get_all_booking_room']

        for index in get_all_booking_room:
            col = 0
            for subindex in index:
                worksheet.write(row, col, str(subindex))
                col = col + 1
            row = row + 1

    workbook.close()
    return send_file('demo.xlsx', as_attachment=True)

@generate_xlsx_blueprint.route('/get-excel-current-page/<int:current_page_url>')
def download_xlsx_current_page(current_page_url = 1):
    return 'a'