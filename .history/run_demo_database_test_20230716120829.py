# Import library
from models.models import *
import datetime

# Note : This file not public in the server or git hub. This file just use in local.
# This file help create demo database for testing. Please do not edit or delete this file

# This function create 10 records in price table for testing
def ins_new_price_editor_page():
    # value in lst_price is random, not follow any rule, this price
    lst_price = ["30000", "20000", "1000", "5000000", "10000", "250000", "2000500", "150000", "1580000", "2500000"]
    with engine.connect() as conn:
        # I will create 10 record ( 10 rows ) in price table for demo
        for index in range(1,11):
            start_time = datetime.datetime.now()
            end_time = datetime.datetime.now() + datetime.timedelta(days=2)
            price_str = lst_price[index-1]
            ins_new_price = price.insert().values(price = price_str, date_start_price = start_time,
                date_end_price = end_time)
            conn.execute(ins_new_price)
