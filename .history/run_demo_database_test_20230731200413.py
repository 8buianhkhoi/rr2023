# Import library
from models.models import *
import datetime
import platform

# Note : This file not public in the server or git hub. This file just use in local.
# This file help create demo database for testing. Please do not edit or delete this file

# This function create 10 records in price table for demo database
def ins_new_price_editor_page():
    # value in lst_price is random, not follow any rule, this price
    lst_price = ["30000", "20000", "1000", "5000000", "10000", "250000", "2000500", "150000", "1580000", "2500000"]
    with engine.connect() as conn:
        # I will create 10 record ( 10 rows ) in price table for demo
        for index in range(1,11):
            start_time = datetime.datetime.now()
            end_time = datetime.datetime.now() + datetime.timedelta(days=2)
            price_str = lst_price[index-1]
            ins_new_price = price.insert().values(price = price_str, date_start_price = start_time,
                date_end_price = end_time, full_name = f'p{index}')
            conn.execute(ins_new_price)
            if platform.system() == 'Linux':
                conn.commit()

# This function create 10 records in duration table for demo database
def ins_new_duration_editor_page():
    lst_duration = ['50000', '72000', '79999', '80000', '20000', '1400000', '2000000', '500000', '90000', '100000']
    with engine.connect() as conn:
        for index in range(1, 11):
            start_time = datetime.datetime.now()
            end_time = datetime.datetime.now() + datetime.timedelta(days=2)
            duration_str = lst_duration[index-1]
            ins_new_duration = duration.insert().values(duration = duration_str, date_start_duration = start_time,
                date_end_duration = end_time, full_name = f'd{index}')
            conn.execute(ins_new_duration)
            if platform.system() == 'Linux':
                conn.commit()

# This function try to create 100 booking room demo
def ins_100_booking_room():
    with engine.connect() as conn:
        id_user_para = 7
        lst_id_all_room = [1, 2, 3, 4]
        default_service_para = {"TV" : 2}
        option_service_para = {"Tủ lạnh" : 2}
        status_para = 'Pending'
        code_booking_para = datetime.datetime.now().strftime(f'%Y%m%d%H%M%S') + '_' + str(id_user_para) + "_"

# ins_new_price_editor_page()
# ins_new_duration_editor_page()
