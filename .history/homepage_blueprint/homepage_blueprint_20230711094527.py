# Import library
from flask import Blueprint, render_template

homepage_blueprint = Blueprint('HP_bp', __name__, static_folder = 'static', template_folder = 'templates')

@homepage_blueprint.route('/')
@homepage_blueprint.route('/homepage')
def homepage_page():
    return render_template('homepage_page.html')