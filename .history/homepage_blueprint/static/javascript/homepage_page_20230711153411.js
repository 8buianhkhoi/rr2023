function zoom_room_image(img){
    // 400px here is the same width with 400px in css. If you want to change here you need to change in css. class is : each__images__room
    let width_image = '400px';

    if (img.style.width === width_image) {
        img.style.width = '1000px';
        img.style.z_index : '1'
    }
    else {
        img.style.width = width_image;
    }
}