const each_images = document.querySelectorAll('.images__about__us')
let current_images = 0

function slide_show_images_about_us(index){
    each_images.forEach((each_image, i)=>{
        if (i === index){
            each_image.classList.add('active__images__about__us')
        }
        else {
            each_image.classList.remove('active__images__about__us')
        }
    })
}

function next_image(){
    current_images = (current_images + 1) % each_images.length;
    console.log(document.querySelectorAll('.images__about__us').length)
    slide_show_images_about_us(current_images)
}

setInterval(next_image, 3000)