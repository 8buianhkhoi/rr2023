document.addEventListener('DOMContentLoaded', function(){
    let slide_index = 0;
    slide_show_images_about_us()
    
    function slide_show_images_about_us(){
        let i;
        let x = document.getElementsByClassName('images__about__us');
        for (i = 0; i <x.length; i++){
            x[i].style.display = 'none'
        }
        slide_index++;
        if (slide_index > x.length) { slide_index = 1}
        x[slide_index - 1].style.display = 'block'
        setTimeout(slide_show_images_about_us, 2000)
    }
})

function showSmallTypeRoomDetail(){
    document.getElementsByClassName('type__room__small__detail')[0].style.display = 'block';
    document.getElementsByClassName('type__room__small__detail')[0].style.transform = 'translateX(0)'
}

function closeRoomSmallDetail() {
    document.getElementsByClassName('type__room__small__detail')[0].style.display = 'none'
    document.getElementsByClassName('type__room__small__detail')[0].style.height = '0'
}