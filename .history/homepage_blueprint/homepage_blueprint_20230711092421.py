# Import library
from flask import Blueprint

homepage_blueprint = Blueprint('HP_bp', __name__, static_folder = 'static', template_folder = 'templates')