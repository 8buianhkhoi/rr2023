from flask import Blueprint, render_template, session, redirect, url_for, request
from firebase_admin import storage
import ast
import base64
from datetime import datetime, timedelta
import json
from dotenv import load_dotenv
import os
import jwt

from . import exec_data

user_blueprint = Blueprint('USER_bp', __name__, static_folder = 'static', template_folder = 'templates')

@user_blueprint.before_request
def check_current_token():
    if 'token_rr_2023' not in session:
        return redirect(url_for('Login_bp.login_account'))
        
    load_dotenv()
    secret_key = os.getenv('secret_key')
    decode_token = jwt.decode(session['token_rr_2023'], secret_key, algorithms = ["HS256"])
    current_time = datetime.now()
    end_time_token = datetime.strptime(decode_token['end_time_token'], f'%d-%m-%Y %H:%M:%S')

    if current_time < end_time_token:
        role_user_temp = decode_token['role_user']
        if role_user_temp == 'user':
            session['role_user_rr_2023'] = 'user'
            session['id_user'] = decode_token['id_login']
        else:
            return "<p>You are not login at role user, so you can't access this page</p>"
    else:
        return redirect(url_for('Login_bp.login_account'))

def download_img_from_firebase(remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    image_data = blob.download_as_bytes()
    base64_img = base64.b64encode(image_data).decode()
    return base64_img

@user_blueprint.route('/log-out-user')
def log_out_user():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))
 
@user_blueprint.route('/homepage-user/type/<type_room>')
def type_room_homepage(type_room = None):
    return_para = {}
    lst_base64_image_firebase = []

    if type_room == 'big':
        get_type_room = exec_data.get_type_room(type_room_para = 'big')
    elif type_room == 'medium':
        get_type_room = exec_data.get_type_room(type_room_para = 'medium')
    elif type_room == 'small':
        get_type_room = exec_data.get_type_room(type_room_para = 'small')
    elif type_room == 'all':
        get_type_room = exec_data.get_type_room(type_room_para = 'all')
    else:
        return "Đường dẫn sai rồi nè :v"
    
    if get_type_room != False:
        return_para['get_type_room'] = get_type_room['get_type_room']
        return_para['lst_price'] = get_type_room['lst_price']

    # for each_image_firebase in get_type_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('type_room_homepage.html', **return_para)

@user_blueprint.route('/homepage-user/addr/<province_para>')
def province_room_homepage(province_para):
    return_para = {}
    lst_base64_image_firebase = []
    get_province_room = exec_data.get_room_by_province(province_para)

    if get_province_room != False:
        return_para['province_room'] = get_province_room['get_room_by_province']
        return_para['lst_price'] = get_province_room['lst_price']

    # for each_image_firebase in get_province_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('province_room_homepage.html', **return_para)


# This route show information about the room, show detail and all information about one room
# each room have each code random separate, so we use code_random for get information from database
@user_blueprint.route('/details-room/<code_room>', methods = ['GET', 'POST'])
def detail_room(code_room):
    if request.method == 'POST':
        if 'submit__booking__room' in request.form:
            date_rent_room = request.form['choose_date_rent_room']
            second_rent_room = request.form['second_rent_room']
            id_rent_room = request.form['choose_id_rent_room']
            id_user = session['id_user']
            dict_default_service = ast.literal_eval(request.form['choose_default_service_dict']) 
            dict_option_service = ast.literal_eval(request.form['choose_option_service_dict'])
            
            ins_booking_room = exec_data.post_new_booking_room(id_user_para = id_user, id_all_room_para = id_rent_room,
                time_start_para = date_rent_room, default_service_para = dict_default_service, 
                option_service_para = dict_option_service, second_rent_para = second_rent_room)
            
            if ins_booking_room == True:
                return redirect(url_for('USER_bp.detail_room', code_room = code_room, msg_ins_new_booking_room = True))
            else:
                return redirect(url_for('USER_bp.detail_room', code_room = code_room, msg_ins_new_booking_room = False))

    return_para = {}  

    # Request.args.get try to find parameter msg_ins_new_booking_room, if not defined return None otherwise return msg_ins_new_booking_room para
    msg_ins_new_booking_room = request.args.get('msg_ins_new_booking_room')
    if msg_ins_new_booking_room is not None:
        if msg_ins_new_booking_room == 'True':
            return_para['success_ins_new_booking_room'] = 'Đặt phòng họp thành công'
        else:
            return_para['fail_ins_new_booking_room'] = 'Đặt phòng họp thất bại'


    get_room_by_code_room = exec_data.get_detail_room_by_code_room(code_room)
    get_duration_room = get_room_by_code_room['get_duration_room']

    # each room in database have id_duration column, foreign key to duration table
    # 1 duration have start time and end time for use this room
    # 1 room can use in some seconds
    # so we need to check the room, if the room in valid duration, ok , can rent the room, otherwise not rent room 
    start_time_duration_room = get_duration_room[2]
    end_time_duration_room = get_duration_room[3]
    second_duration_room = int(get_duration_room[1])
    current_time_now = datetime.now() + timedelta(seconds = second_duration_room)
    
    if (start_time_duration_room <= current_time_now <= end_time_duration_room) == True:
        return_para['valid_duration_room'] = True
        return_para['start_time_duration_room'] = start_time_duration_room
        return_para['end_time_duration_room'] = end_time_duration_room
        return_para['second_duration_room'] = get_duration_room[1]
        return_para['default_service_dict'] = get_room_by_code_room['default_service_dict']
        return_para['option_service_dict'] = get_room_by_code_room['option_service_dict']
    else:
        return_para['valid_duration_room'] = False

    if get_room_by_code_room != False:
        return_para['get_room_by_code_room'] = get_room_by_code_room['get_room_by_code_room']
        return_para['price_room'] = get_room_by_code_room['price_room'][1]
        return_para['lst_name_image_firebase'] = get_room_by_code_room['lst_name_image_firebase']
    
    # ast.literal_eval
    return render_template('detail_room.html', **return_para)

@user_blueprint.route('/valid-rent-room/type/<type_room>')
def valid_rent_room(type_room = None):
    return_para = {}

    get_valid_rent_room = exec_data.get_valid_duration_room()
    return_para['lst_valid_duration_room'] = get_valid_rent_room['lst_valid_duration_room']
    return_para['lst_price'] = get_valid_rent_room['lst_price']
    return_para['lst_name_image_firebase'] = get_valid_rent_room['lst_name_image_firebase']

    return render_template('valid_rent_room_type.html', **return_para)