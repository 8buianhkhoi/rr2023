from sqlalchemy import select

from models.models import *

def get_type_room(type_room_para):
    try:
        with engine.connect() as conn:
            lst_price = []  
            lst_name_image_firebase = []
            print(type_room_para)
            if type_room_para == "all":
                query_get_type_room = select(all_room)
            elif type_room_para == 'small':
                query_get_type_room = select(all_room).where(all_room.c.type == 'small')
            elif type_room_para == 'medium':
                query_get_type_room = select(all_room).where(all_room.c.type == 'medium')
            elif type_room_para == 'big':
                query_get_type_room = select(all_room).where(all_room.c.type == 'big')
            else:
                return False

            get_type_room = conn.execute(query_get_type_room).fetchall()

            for index in query_get_type_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

            return {'get_type_room' : get_type_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to find all room match address province
def get_room_by_province(province_para):
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []

            if province_para == 'ha_noi_vn':
                query_get_room_ha_noi = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
                get_room = conn.execute(query_get_room_ha_noi).fetchall()
            elif province_para == 'ho_chi_minh_vn':
                query_get_room_ho_chi_minh = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
                get_room = conn.execute(query_get_room_ho_chi_minh).fetchall()
            elif province_para == 'da_nang_vn':
                query_get_room_da_nang = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
                get_room = conn.execute(query_get_room_da_nang).fetchall()

            for index in get_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'get_room_by_province' : get_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False
