from sqlalchemy import select

from models.models import *

def get_all_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()

            for index in get_all_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'all_room' : get_all_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_small_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_small_room = select(all_room).where(all_room.c.type == 'small')
            get_small_room = conn.execute(query_get_small_room).fetchall()

            for index in get_small_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'small_room' : get_small_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

    