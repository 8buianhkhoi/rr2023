from sqlalchemy import select

from models.models import *

def get_type_room(type_room_para):
    try:
        with engine.connect() as conn:
            lst_price = []  
            lst_name_image_firebase = []
            
            if type_room_para == "all":
                query_get_type_room = select(all_room)
            elif type_room_para == 'small':
                query_get_type_room = select(all_room).where(all_room.c.type == 'small')
            elif type_room_para == 'medium':
                query_get_type_room = select(all_room).where(all_room.c.type == 'medium')
            elif type_room_para == 'big':
                query_get_type_room = select(all_room).where(all_room.c.type == 'big')
            else:
                return False

            get_type_room = conn.execute(query_get_type_room).fetchall()

            for index in get_type_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

            return {'get_type_room' : get_type_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to find all room match address province
def get_room_by_province(province_para):
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []

            if province_para == 'ha_noi_vn':
                query_get_room_ha_noi = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
                get_room = conn.execute(query_get_room_ha_noi).fetchall()
            elif province_para == 'ho_chi_minh_vn':
                query_get_room_ho_chi_minh = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
                get_room = conn.execute(query_get_room_ho_chi_minh).fetchall()
            elif province_para == 'da_nang_vn':
                query_get_room_da_nang = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
                get_room = conn.execute(query_get_room_da_nang).fetchall()

            for index in get_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'get_room_by_province' : get_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_detail_room_by_code_room(code_room_para):
    try:
        with engine.connect() as conn:
            query_get_room = select(all_room).where(all_room.c.code_room == code_room_para)
            get_room_by_code_random = conn.execute(query_get_room).fetchone()

            id_price = get_room_by_code_random[10]
            id_duration = get_room_by_code_random[9]

            query_get_price = select(price).where(price.c.id == id_price)
            query_get_duration = select(duration).where(duration.c.id == id_duration)

            get_price_room = conn.execute(query_get_price).fetchone()
            get_duration_room = conn.execute(query_get_duration).fetchone()

        return {'get_room_by_code_room' : get_room_by_code_random, 'price_room' : get_price_room,
            'lst_name_image_firebase' : get_room_by_code_random[13], 'get_duration_room' : get_duration_room}
    except:
        return False

# This function create a new booking room.
def post_new_booking_room(id_room_para, id_user_booking_room):
    try:
        with engine.connect() as conn:
            #id_room_para is a id save in database of all_room table
            # each room hay id different
    except:
        return False
