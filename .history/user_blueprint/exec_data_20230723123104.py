from sqlalchemy import select

from models.models import *

def get_all_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()

            for index in get_all_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)

        return {'all_room' : get_all_room, 'lst_price' : lst_price}
    except:
        return False

    