from sqlalchemy import select

from models.models import *

def get_all_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()

            for index in get_all_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'all_room' : get_all_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_small_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_small_room = select(all_room).where(all_room.c.type == 'small')
            get_small_room = conn.execute(query_get_small_room).fetchall()

            for index in get_small_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'small_room' : get_small_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_medium_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_medium_room = select(all_room).where(all_room.c.type == 'medium')
            get_medium_room = conn.execute(query_get_medium_room).fetchall()

            for index in get_medium_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'medium_room' : get_medium_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_big_room():
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []
            query_get_big_room = select(all_room).where(all_room.c.type == 'big')
            get_big_room = conn.execute(query_get_big_room).fetchall()

            for index in get_big_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'big_room' : get_big_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to find all room match address province
def get_room_by_province(province_para):
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []

            if province_para == 'ha_noi_vn':
                query_get_room_ha_noi = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
                get_room = conn.execute(query_get_room_ha_noi).fetchall()
            elif province_para == 'ho_chi_minh_vn':
                query_get_room_ho_chi_minh = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
                get_room = conn.execute(query_get_room_ho_chi_minh).fetchall()
            elif province_para == 'da_nang_vn':
                query_get_room_da_nang = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
                get_room = conn.execute(query_get_room_da_nang).fetchall()

            for index in get_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'get_room_by_province' : get_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False
    