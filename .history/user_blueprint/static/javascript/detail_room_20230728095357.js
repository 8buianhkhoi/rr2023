var dict_default_service = {}
var dict_option_service = {}

function choose_date_rent_room_js() {
    let choose_time_rent_room = document.getElementsByClassName('choose__date__rent__room')[0].value
    let choose_second_rent_room = document.getElementsByClassName('choose__duration__rent__room')[0].value

    if ((choose_time_rent_room === '') || (choose_second_rent_room === '')){

    }
    else{
        let choose_time = new Date(choose_time_rent_room)
        choose_time.setSeconds(choose_time.getSeconds() + parseInt(choose_second_rent_room))
        let start_time = new Date(start_time_duration_room)
        let end_time = new Date(end_time_duration_room)

        if ( start_time <= choose_time && choose_time <= end_time){
            document.getElementsByClassName('valid__condition__rent__room__title')[0].innerText =
             `Bạn có thể thuê. Thời gian thuê bắt đầu từ ${new Date(choose_time_rent_room)} tới ${choose_time} `
            document.getElementById('btn__submit__booking__room').style.display = 'inline-block'
        }
        else{
            document.getElementsByClassName('valid__condition__rent__room__title')[0].innerText = 'Bạn không thể thuê vì chưa đủ điều kiện thời gian'
            document.getElementById('btn__submit__booking__room').style.display = 'none'
        }
    }
}

function plus_amount_default_service(name_default_service_para){
    if (dict_default_service[name_default_service_para] < dict_default_service_room[name_default_service_para]){
        dict_default_service[name_default_service_para] += 1}
    else{
        alert('Bạn đã thêm quá giới hạn cho phép')
    }
    show_default_service_room()
}

function minus_amount_default_service(name_default_service_para){
    if (dict_default_service[name_default_service_para] === 1){
        confirm_delete_default_service(name_default_service_para)
    }
    else{
        dict_default_service[name_default_service_para] -= 1
        show_default_service_room()
    }
}

function delete_default_service(name_default_service_para){
    delete dict_default_service[name_default_service_para]
    remove_confirm_delete_default_service()
    show_default_service_room()
}

function remove_confirm_delete_default_service(){
    document.getElementsByClassName('pop__up__confirm__delete')[0].remove()
}

function confirm_delete_default_service(name_default_service_para){
    let new_div_tag = document.createElement('div')
    let new_h3_tag = document.createElement('h3')
    let new_sub_div_tag = document.createElement('div')
    let new_delete_btn = document.createElement('button')
    let new_cancel_delete_btn = document.createElement('button')

    new_div_tag.setAttribute('class', 'pop__up__confirm__delete')
    new_h3_tag.setAttribute('class', 'title__pop__up__confirm__delete')
    new_delete_btn.setAttribute('class', 'accept__delete__service')
    new_cancel_delete_btn.setAttribute('class', 'disaccept__delete__service')
    new_delete_btn.setAttribute('type', 'button')
    new_cancel_delete_btn.setAttribute('type', 'button')

    new_h3_tag.textContent = 'Bạn có muốn xóa không ?'
    new_delete_btn.textContent = 'Xóa'
    new_cancel_delete_btn.textContent = 'Không'

    new_sub_div_tag.append(new_delete_btn)
    new_sub_div_tag.append(new_cancel_delete_btn)

    new_div_tag.append(new_h3_tag)
    new_div_tag.append(new_sub_div_tag)

    new_delete_btn.setAttribute('onclick', `delete_default_service("${name_default_service_para}")`)
    new_cancel_delete_btn.setAttribute('onclick', `remove_confirm_delete_default_service()`)

    document.querySelector('body').appendChild(new_div_tag)
}

function show_default_service_room(){
    document.getElementsByClassName('lst__default__service__add__room')[0].innerHTML = ''

    for( let default_service in dict_default_service){
        let new_div_tag = document.createElement('div')
        let new_name_service = document.createElement('p')

        let new_amount_div_tag = document.createElement('div')
        let new_plus_amount_btn = document.createElement('button')
        let new_amount_text = document.createElement('p')
        let new_minus_amount_btn = document.createElement('button')

        let new_close_btn = document.createElement('button')

        new_name_service.textContent = default_service
        new_plus_amount_btn.textContent = '+'
        new_amount_text.textContent = dict_default_service[default_service]
        new_minus_amount_btn.textContent = '-'
        new_close_btn.textContent = 'X'

        new_plus_amount_btn.setAttribute('onclick', `plus_amount_default_service("${default_service}")`)
        new_plus_amount_btn.setAttribute('type', 'button')
        new_minus_amount_btn.setAttribute('type', 'button')
        new_close_btn.setAttribute('type', 'button')
        new_minus_amount_btn.setAttribute('onclick', `minus_amount_default_service("${default_service}")`)
        new_close_btn.setAttribute('onclick', `confirm_delete_default_service("${default_service}")`)

        new_amount_div_tag.append(new_minus_amount_btn)
        new_amount_div_tag.append(new_amount_text)
        new_amount_div_tag.append(new_plus_amount_btn)

        new_div_tag.append(new_name_service)
        new_div_tag.append(new_amount_div_tag)
        new_div_tag.append(new_close_btn)

        document.getElementsByClassName('lst__default__service__add__room')[0].appendChild(new_div_tag)
    }
    document.getElementsByClassName('choose__default__service__dict')[0].value = JSON.stringify(dict_default_service)
}

function add_default_service(){
    let default_service_text = document.getElementsByClassName('select__choose__default__service')[0].value

    if (default_service_text in dict_default_service){
        if (dict_default_service[default_service_text] < dict_default_service_room[default_service_text]){
            dict_default_service[default_service_text] += 1}
        else{
            alert('Bạn đã thêm quá giới hạn cho phép')
        }
    }
    else{
        dict_default_service[default_service_text] = 1
    }
    show_default_service_room()
}

// option service

function plus_amount_option_service(name_option_service_para){
    if (dict_option_service[name_option_service_para]['amount'] < dict_option_service_room[name_option_service_para]['amount']){
        dict_option_service[name_option_service_para]['amount'] += 1}
    else{
        alert('Bạn đã thêm quá giới hạn cho phép')
    }
    show_option_service_room()
}

function minus_amount_option_service(name_option_service_para){
    if (dict_option_service[name_option_service_para]['amount'] === 1){
        confirm_delete_option_service(name_option_service_para)
    }
    else{
        dict_option_service[name_option_service_para]['amount'] -= 1
        show_option_service_room()
    }
}

function delete_option_service(name_option_service_para){
    delete dict_option_service[name_option_service_para]
    remove_confirm_delete_option_service()
    show_option_service_room()
}

function remove_confirm_delete_option_service(){
    document.getElementsByClassName('pop__up__confirm__delete')[0].remove()
}

function confirm_delete_option_service(name_option_service_para){
    let new_div_tag = document.createElement('div')
    let new_h3_tag = document.createElement('h3')
    let new_sub_div_tag = document.createElement('div')
    let new_delete_btn = document.createElement('button')
    let new_cancel_delete_btn = document.createElement('button')

    new_div_tag.setAttribute('class', 'pop__up__confirm__delete')
    new_h3_tag.setAttribute('class', 'title__pop__up__confirm__delete')
    new_delete_btn.setAttribute('class', 'accept__delete__service')
    new_cancel_delete_btn.setAttribute('class', 'disaccept__delete__service')
    new_delete_btn.setAttribute('type', 'button')
    new_cancel_delete_btn.setAttribute('type', 'button')

    new_h3_tag.textContent = 'Bạn có muốn xóa không ?'
    new_delete_btn.textContent = 'Xóa'
    new_cancel_delete_btn.textContent = 'Không'

    new_sub_div_tag.append(new_delete_btn)
    new_sub_div_tag.append(new_cancel_delete_btn)

    new_div_tag.append(new_h3_tag)
    new_div_tag.append(new_sub_div_tag)

    new_delete_btn.setAttribute('onclick', `delete_option_service("${name_option_service_para}")`)
    new_cancel_delete_btn.setAttribute('onclick', `remove_confirm_delete_option_service()`)

    document.querySelector('body').appendChild(new_div_tag)
}

function show_option_service_room(){
    document.getElementsByClassName('lst__option__service__add__room')[0].innerHTML = ''

    for( let option_service in dict_option_service){
        let new_div_tag = document.createElement('div')
        let new_name_service = document.createElement('p')

        let new_amount_div_tag = document.createElement('div')
        let new_plus_amount_btn = document.createElement('button')
        let new_amount_text = document.createElement('p')
        let new_minus_amount_btn = document.createElement('button')

        let new_close_btn = document.createElement('button')

        new_name_service.textContent = option_service
        new_plus_amount_btn.textContent = '+'
        new_amount_text.textContent = dict_option_service[option_service]['amount']
        new_minus_amount_btn.textContent = '-'
        new_close_btn.textContent = 'X'

        new_plus_amount_btn.setAttribute('onclick', `plus_amount_option_service("${option_service}")`)
        new_plus_amount_btn.setAttribute('type', 'button')
        new_minus_amount_btn.setAttribute('type', 'button')
        new_close_btn.setAttribute('type', 'button')
        new_minus_amount_btn.setAttribute('onclick', `minus_amount_option_service("${option_service}")`)
        new_close_btn.setAttribute('onclick', `confirm_delete_option_service("${option_service}")`)

        new_amount_div_tag.append(new_minus_amount_btn)
        new_amount_div_tag.append(new_amount_text)
        new_amount_div_tag.append(new_plus_amount_btn)

        new_div_tag.append(new_name_service)
        new_div_tag.append(new_amount_div_tag)
        new_div_tag.append(new_close_btn)

        document.getElementsByClassName('lst__option__service__add__room')[0].appendChild(new_div_tag)
    }
    document.getElementsByClassName('choose__option__service__dict')[0].value = JSON.stringify(dict_option_service)

    document.getElementsByClassName('price__each__option__service')[0].innerHTML = ''
    let total_price_option_service = 0
    for (let option_service in dict_option_service) {
        let amount_each_option_service = dict_option_service[option_service]['amount']
        let price_each_option_service = parseInt(dict_option_service[option_service]['price'])
        let total_price_each_option_service = amount_each_option_service * price_each_option_service
        total_price_option_service = total_price_option_service + total_price_each_option_service

        let new_div_tag = document.createElement('div')
        let new_name_option_service = document.createElement('p')
        let new_amount_option_service = document.createElement('p')
        let new_total_price_option_service = document.createElement('p')

        new_name_option_service.textContent = 'Name : ' + option_service
        new_amount_option_service.textContent = 'Amount : ' + String(amount_each_option_service)
        new_total_price_option_service.textContent = 'Total Price : ' + String(total_price_each_option_service)

        new_div_tag.append(new_name_option_service)
        new_div_tag.append(new_amount_option_service)
        new_div_tag.append(new_total_price_option_service)

        document.getElementsByClassName('price__each__option__service')[0].appendChild(new_div_tag)
    }
    document.getElementsByClassName('price__option__service')[0].innerText = String(total_price_option_service)
}

function add_option_service(){
    let option_service_text = document.getElementsByClassName('select__choose__option__service')[0].value
    
    if (option_service_text in dict_option_service){
        if (dict_option_service[option_service_text]['amount'] < dict_option_service_room[option_service_text]['amount']){
            dict_option_service[option_service_text]['amount'] += 1}
        else{
            alert('Bạn đã thêm quá giới hạn cho phép')
        }
    }
    else{
        dict_option_service[option_service_text] = {'amount' : 1, 'price' : dict_option_service_room[option_service_text]['price']}
    }
    show_option_service_room()
}