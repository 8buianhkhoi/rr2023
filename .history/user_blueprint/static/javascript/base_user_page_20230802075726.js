window.onload = function(){
    let left_div = document.getElementsByClassName('left__content')[0]
    let right_div = document.getElementsByClassName('right__content')[0]

    left_div.addEventListener('scroll', function(){
        right_div.style.transform =  'translateY(' + left_div.scrollTop + 'px)'
    })
}