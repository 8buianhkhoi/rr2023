function on_page_load_pagination(){
    document.getElementsByClassName('form__btn__static')[0].style.display = 'none';
    if (pagination_page > 5){
        let left_pagination = 2;
        let right_pagination = 2;
        
        if ((current_page - left_pagination < 1) && (CurrentPage + right_pagination <= pagination_page)){
            if (CurrentPage - left_pagination === 0) {
                left_pagination = left_pagination - 1
                right_pagination = right_pagination + 1
            }
            else if (CurrentPage - left_pagination === -1) {
                left_pagination = left_pagination - 2
                right_pagination = right_pagination + 2
                document.getElementById('btn__previous__pagination').style.display = 'none'
                document.getElementById('btn__first__pagination__page').style.display = 'none'
            }
        }
        else if ((CurrentPage - left_pagination >= 1) && (CurrentPage + right_pagination > pagination_page)){
            if (CurrentPage + right_pagination === pagination_page + 1){
                left_pagination = left_pagination + 1
                right_pagination = right_pagination -1
            }
            else if (CurrentPage + right_pagination === pagination_page + 2){
                left_pagination = left_pagination + 2
                right_pagination = right_pagination -2
                document.getElementById('btn__next__pagination').style.display = 'none'
                document.getElementById('btn__end__pagination__page').style.display = 'none'
            }
        }
        let allBtnPagination = document.getElementsByClassName('btn__pagination');
        
        for(let index = 0; index < allBtnPagination.length; index++){
            allBtnPagination[index].style.display = 'none'
        }

        document.getElementById(`btn__page__pagination__page${CurrentPage}`).style.display = 'inline-block';
        
        for (let index = 0; index < leftPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage - (index + 1)}`).style.display = 'inline-block';
        }
        for (let index = 0; index < rightPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage + (index + 1)}`).style.display = 'inline-block';
        }
    }
}

function previousBtnPagination(){
    CurrentPage = CurrentPage - 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function nextBtnPagination(){
    CurrentPage = CurrentPage + 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function firstPagePagination(){
    CurrentPage = 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function lastPagePagination(){
    CurrentPage = pagination_page;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}