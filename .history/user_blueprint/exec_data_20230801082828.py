from sqlalchemy import select
import ast
from datetime import datetime
import platform

from models.models import *

"""
Note:
- Service have two type: Default Service and Option Service
- Each room can have default service or option service or both
- default service have price 0 base price room, so we need to take care about amount of service in room
- option service we have different price
Default service will store in code and assign variable like : default_service = {'a':1}, a is name default service, 1 is amount
Option service will store like {'a' : {'amount' : 1, 'price' : '1000'}}, a is name
"""

"""
We use ast library to convert a string to dictionary
- Sometime get value we get string like "{'a' : 1}". This value is string, we want to convert to dictionary
- We use built-in library ast and literal_eval to convert
Ex : ast.literal_eval("{'a' : 1}")
"""

# This function check OS. Linux os need to do something more than Windows and Darwin ( MacOS )
# In this function if linux os, need to add commit function for execute database ( insert, delete, update)
def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

# This function have input is a list of room. Output is a list price match with list of room
def get_price_lst_room(lst_room_para):
    lst_price = []

    for index in lst_room_para:
        id_price = index[10]
        query_get_price = select(price).where(price.c.id == id_price)
        get_each_price = conn.execute(query_get_price).fetchone()
        lst_price.append(get_each_price)
    
    return lst_price

# This function try to get name image of firebase store in all_room table
def get_img_name_firebase(lst_room_para):
    lst_name_image_firebase = []

    for index in lst_room_para:
        lst_name_image_firebase.append(index[13])
    
    return lst_name_image_firebase

# We have all room in database, we need to load all information about all room in database match with type ( size the room )
# This function don't care the room is available or not, just load all room
# In all_room table we foreign key id price and id duration, so we need to get duration and price in loop for and store it in list
def get_type_room(type_room_para):
    try:
        with engine.connect() as conn:
            if type_room_para == "all":
                query_get_type_room = select(all_room)
            elif type_room_para == 'small':
                query_get_type_room = select(all_room).where(all_room.c.type == 'small')
            elif type_room_para == 'medium':
                query_get_type_room = select(all_room).where(all_room.c.type == 'medium')
            elif type_room_para == 'big':
                query_get_type_room = select(all_room).where(all_room.c.type == 'big')
            else:
                return False

            get_type_room = conn.execute(query_get_type_room).fetchall()
            lst_price = get_price_lst_room(get_type_room)
            lst_name_image_firebase = get_img_name_firebase(get_type_room)

            return {'get_type_room' : get_type_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to find all room match address province
# In this demo we just defined 3 province
# Name province we want to pass parameter like camel case, and each word we use underscore, and add vn in end
# For ex, Ho Chi Minh city, Ho Chi Minh is name, camel case and lower case, add underscore, add vn in end -> ho_chi_minh_vn
def get_room_by_province(province_para):
    try:
        with engine.connect() as conn:
            if province_para == 'ha_noi_vn':
                query_get_room_ha_noi = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
                get_room = conn.execute(query_get_room_ha_noi).fetchall()
            elif province_para == 'ho_chi_minh_vn':
                query_get_room_ho_chi_minh = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
                get_room = conn.execute(query_get_room_ho_chi_minh).fetchall()
            elif province_para == 'da_nang_vn':
                query_get_room_da_nang = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
                get_room = conn.execute(query_get_room_da_nang).fetchall()

            lst_price = get_price_lst_room(get_room)
            lst_name_image_firebase = get_img_name_firebase(get_room)

        return {'get_room_by_province' : get_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# Each room in database have different code, we use this code for some purpose
# code column have unique datatype
def get_detail_room_by_code_room(code_room_para):
    try:
        with engine.connect() as conn:
            query_get_room = select(all_room).where(all_room.c.code_room == code_room_para)
            get_room_by_code_random = conn.execute(query_get_room).fetchone()

            default_service_dict = ast.literal_eval(get_room_by_code_random.default_service)
            option_service_dict = ast.literal_eval(get_room_by_code_random.option_service)

            for option_service in option_service_dict:
                query_select_id_price = select(option_service_room).where(option_service_room.c.name == option_service)
                id_price_option_service = conn.execute(query_select_id_price).fetchone()[2]

                query_select_price = select(price).where(price.c.id == id_price_option_service)
                get_price_option_service = conn.execute(query_select_price).fetchone()

                start_time_price = get_price_option_service[2]
                end_time_price = get_price_option_service[3]
                current_time_now = datetime.now()

                if start_time_price <= current_time_now <= end_time_price:
                    price_option_service = get_price_option_service[1]
                else:
                    price_option_service = None

                option_service_dict[option_service] = {'amount': option_service_dict[option_service], 'price' : price_option_service}

            id_price = get_room_by_code_random[10]
            id_duration = get_room_by_code_random[9]

            query_get_price = select(price).where(price.c.id == id_price)
            query_get_duration = select(duration).where(duration.c.id == id_duration)

            get_price_room = conn.execute(query_get_price).fetchone()
            get_duration_room = conn.execute(query_get_duration).fetchone()

        return {'get_room_by_code_room' : get_room_by_code_random, 'price_room' : get_price_room,
            'lst_name_image_firebase' : get_room_by_code_random[13], 'get_duration_room' : get_duration_room,
            'default_service_dict' : default_service_dict, 'option_service_dict' : option_service_dict}
    except:
        return False

# This function try to get all room in available, in simple case, in this project we just care about the room available duration
def get_valid_duration_room_type(type_room_para):
    try:
        with engine.connect() as conn:
            lst_valid_duration_room = []
            lst_price = []
            lst_name_image_firebase = []

            if type_room_para == 'all':
                query_get_all_room = select(all_room)
            elif type_room_para == 'small':
                query_get_all_room = select(all_room).where(all_room.c.type == 'small')
            elif type_room_para == 'medium':
                query_get_all_room = select(all_room).where(all_room.c.type == 'medium')
            elif type_room_para == 'big':
                query_get_all_room = select(all_room).where(all_room.c.type == 'big')
            else:
                return False

            get_all_room = conn.execute(query_get_all_room).fetchall()
            
            for each_room in get_all_room:
                id_duration_each_room = each_room.id_duration
                id_each_room = each_room.id
                
                query_each_duration = select(duration).where(duration.c.id == id_duration_each_room)
                each_duration = conn.execute(query_each_duration).fetchone()

                start_time_duration = each_duration.date_start_duration
                end_time_duration = each_duration.date_end_duration
                current_time = datetime.now()

                if start_time_duration < current_time < end_time_duration:
                    lst_valid_duration_room.append(each_room)
            
            lst_price = get_price_lst_room(lst_valid_duration_room)
            lst_name_image_firebase = get_img_name_firebase(lst_valid_duration_room)
        return {'lst_valid_duration_room' : lst_valid_duration_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_valid_duration_room_province(province_para):
    try:
        with engine.connect() as conn:
            lst_valid_duration_room = []
            lst_price = []
            lst_name_image_firebase = []

            if province_para == 'ho_chi_minh_vn':
                query_get_all_room = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
            elif province_para == 'ha_noi_vn':
                query_get_all_room = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
            elif province_para == 'da_nang_vn':
                query_get_all_room = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
            else:
                return False

            get_all_valid_room = conn.execute(query_get_all_room).fetchall()
            
            for each_room in get_all_valid_room:
                id_duration_each_room = each_room.id_duration
                id_each_room = each_room.id
                
                query_each_duration = select(duration).where(duration.c.id == id_duration_each_room)
                each_duration = conn.execute(query_each_duration).fetchone()

                start_time_duration = each_duration.date_start_duration
                end_time_duration = each_duration.date_end_duration
                current_time = datetime.now()

                if start_time_duration < current_time < end_time_duration:
                    lst_valid_duration_room.append(each_room)
            
            lst_price = get_price_lst_room(lst_valid_duration_room)
            lst_name_image_firebase = get_img_name_firebase(lst_valid_duration_room)
        return {'lst_valid_duration_room' : lst_valid_duration_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function create a new booking room.
def post_new_booking_room(id_user_para, id_all_room_para, time_start_para, default_service_para, option_service_para, 
        second_rent_para, total_price_para, option_service_price_para, note_booking_para):
    try:
        with engine.connect() as conn:
            code_booking_para = datetime.now().strftime(f'%Y%m%d%H%M%S') + '_' + str(id_user_para) + "_" + id_all_room_para

            ins_new_booking_room = booking_room.insert().values(id_users = id_user_para, id_all_room = id_all_room_para,
                default_service = default_service_para, option_service = option_service_para, time_booking = datetime.now(),
                time_start = time_start_para, second_rent = second_rent_para, status = 'Pending', total_price = total_price_para,
                option_service_price = option_service_price_para, note_booking = note_booking_para, code_booking = code_booking_para)
            
            conn.execute(ins_new_booking_room)
            check_current_os(conn)
        return True
    except:
        return False 

# This function try to show all booking room, order by date
def get_all_booking_room():
    try:
        with engine.connect() as conn:
            lst_all_room = []
            lst_price = []
            lst_name_image_firebase = []

            query_get_all_booking_room = select(booking_room).order_by(booking_room.c.time_booking.desc())
            all_booking_room = conn.execute(query_get_all_booking_room).fetchall()

            for each_room in all_booking_room:
                id_each_room = each_room.id_all_room 
                query_get_each_room = select(all_room).where(all_room.c.id == id_each_room)
                get_each_room = conn.execute(query_get_each_room).fetchone()
                lst_all_room.append(get_each_room)

            for index in lst_all_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])
        return {'all_booking_room' : all_booking_room, 'lst_all_room' : lst_all_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to get booking room base code booking
# Each booking have different code
def get_booking_by_code(code_booking_para):
    try:
        with engine.connect() as conn:
            query_get_booking = select(booking_room).where(booking_room.c.code_booking == code_booking_para)
            get_booking = conn.execute(query_get_booking).fetchone()

            query_get_room = select(all_room).where(all_room.c.id == get_booking.id_all_room)
            get_room = conn.execute(query_get_room).fetchone()

        return {'get_booking' : get_booking, 'get_room' : get_room}
    except:
        return False

def post_cancel_rent_room_by_code(code_booking_para):
    try:
        with engine.connect() as conn:
            cancel_rent_room_statement = booking_room.update().where(booking_room.c.code_booking == code_booking_para).values(status = 'Cancel')
            conn.execute(cancel_rent_room_statement)
            check_current_os(conn)
        return True
    except:
        return False

def get_rent_room_cancel():
    try:
        with engine.connect() as conn:
            query_get_cancel_rent_room = select(booking_room).where(booking_room.c.status == 'Cancel')
            get_cancel_rent_room = conn.execute(query_get_cancel_rent_room).fetchall()
        
        return {'get_cancel_rent_room' : get_cancel_rent_room}
    except:
        return False
