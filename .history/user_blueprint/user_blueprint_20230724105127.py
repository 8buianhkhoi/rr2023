from flask import Blueprint, render_template, session, redirect, url_for
from firebase_admin import storage

from . import exec_data

user_blueprint = Blueprint('USER_bp', __name__, static_folder = 'static', template_folder = 'templates')

def download_img_from_firebase(remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    image_data = blob.download_as_bytes()
    base64_img = base64.b64encode(image_data).decode()
    return base64_img

@user_blueprint.route('/homepage-user')
def homepage_user_page():
    return_para = {}
    get_all_room = exec_data.get_all_room()

    if get_all_room != False:
        return_para['all_room'] = get_all_room['all_room']
        return_para['lst_price'] = get_all_room['lst_price']

    return render_template('homepage_user_page.html',**return_para)

@user_blueprint.route('/log-out-user')
def log_out_user():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))