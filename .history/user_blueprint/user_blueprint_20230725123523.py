from flask import Blueprint, render_template, session, redirect, url_for
from firebase_admin import storage
import ast
import base64

from . import exec_data

user_blueprint = Blueprint('USER_bp', __name__, static_folder = 'static', template_folder = 'templates')

def download_img_from_firebase(remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    image_data = blob.download_as_bytes()
    base64_img = base64.b64encode(image_data).decode()
    return base64_img

@user_blueprint.route('/homepage-user/all')
def all_room_homepage():
    return_para = {}
    lst_base64_image_firebase = []
    get_all_room = exec_data.get_all_room()

    if get_all_room != False:
        return_para['all_room'] = get_all_room['all_room']
        return_para['lst_price'] = get_all_room['lst_price']

    # for each_image_firebase in get_all_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('all_room_homepage.html',**return_para)

@user_blueprint.route('/log-out-user')
def log_out_user():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@user_blueprint.route('/homepage-user/small')
def small_room_homepage():
    return_para = {}
    lst_base64_image_firebase = []
    get_small_room = exec_data.get_small_room()
    
    if get_small_room != False:
        return_para['small_room'] = get_small_room['small_room']
        return_para['lst_price'] = get_small_room['lst_price']

    # for each_image_firebase in get_small_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('small_room_homepage.html', **return_para)

@user_blueprint.route('/homepage-user/medium')
def medium_room_homepage():
    return_para = {}
    lst_base64_image_firebase = []
    get_medium_room = exec_data.get_medium_room()
    
    if get_medium_room != False:
        return_para['medium_room'] = get_medium_room['medium_room']
        return_para['lst_price'] = get_medium_room['lst_price']

    # for each_image_firebase in get_medium_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('medium_room_homepage.html', **return_para)

@user_blueprint.route('/homepage-user/big')
def big_room_homepage():
    return_para = {}
    lst_base64_image_firebase = []
    get_big_room = exec_data.get_big_room()
    
    if get_big_room != False:
        return_para['big_room'] = get_big_room['big_room']
        return_para['lst_price'] = get_big_room['lst_price']

    # for each_image_firebase in get_big_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('big_room_homepage.html', **return_para)

@user_blueprint.route('/homepage-user/<province>')
def province_room_homepage(province_para):
    return_para = {}
    lst_base64_image_firebase = []
    get_province_room = exec_data.get_room_by_province(province_para)
    



    
    if get_big_room != False:
        return_para['big_room'] = get_big_room['big_room']
        return_para['lst_price'] = get_big_room['lst_price']

    # for each_image_firebase in get_big_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('big_room_homepage.html', **return_para)