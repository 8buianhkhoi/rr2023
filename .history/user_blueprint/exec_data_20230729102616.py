from sqlalchemy import select
import ast
from datetime import datetime
import platform

from models.models import *

def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

def get_type_room(type_room_para):
    try:
        with engine.connect() as conn:
            lst_price = []  
            lst_name_image_firebase = []
            
            if type_room_para == "all":
                query_get_type_room = select(all_room)
            elif type_room_para == 'small':
                query_get_type_room = select(all_room).where(all_room.c.type == 'small')
            elif type_room_para == 'medium':
                query_get_type_room = select(all_room).where(all_room.c.type == 'medium')
            elif type_room_para == 'big':
                query_get_type_room = select(all_room).where(all_room.c.type == 'big')
            else:
                return False

            get_type_room = conn.execute(query_get_type_room).fetchall()

            for index in get_type_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

            return {'get_type_room' : get_type_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

# This function try to find all room match address province
def get_room_by_province(province_para):
    try:
        with engine.connect() as conn:
            lst_price = []
            lst_name_image_firebase = []

            if province_para == 'ha_noi_vn':
                query_get_room_ha_noi = select(all_room).where(all_room.c.province == 'Thành phố Hà Nội')
                get_room = conn.execute(query_get_room_ha_noi).fetchall()
            elif province_para == 'ho_chi_minh_vn':
                query_get_room_ho_chi_minh = select(all_room).where(all_room.c.province == 'Thành phố Hồ Chí Minh')
                get_room = conn.execute(query_get_room_ho_chi_minh).fetchall()
            elif province_para == 'da_nang_vn':
                query_get_room_da_nang = select(all_room).where(all_room.c.province == 'Thành phố Đà Nẵng')
                get_room = conn.execute(query_get_room_da_nang).fetchall()

            for index in get_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])

        return {'get_room_by_province' : get_room, 'lst_price' : lst_price, 'lst_name_image_firebase' : lst_name_image_firebase}
    except:
        return False

def get_detail_room_by_code_room(code_room_para):
    try:
        with engine.connect() as conn:
            query_get_room = select(all_room).where(all_room.c.code_room == code_room_para)
            get_room_by_code_random = conn.execute(query_get_room).fetchone()

            default_service_dict = ast.literal_eval(get_room_by_code_random.default_service)
            option_service_dict = ast.literal_eval(get_room_by_code_random.option_service)

            for option_service in option_service_dict:
                query_select_id_price = select(option_service_room).where(option_service_room.c.name == option_service)
                id_price_option_service = conn.execute(query_select_id_price).fetchone()[2]

                query_select_price = select(price).where(price.c.id == id_price_option_service)
                get_price_option_service = conn.execute(query_select_price).fetchone()

                start_time_price = get_price_option_service[2]
                end_time_price = get_price_option_service[3]
                current_time_now = datetime.now()

                if start_time_price <= current_time_now <= end_time_price:
                    price_option_service = get_price_option_service[1]
                else:
                    price_option_service = None

                option_service_dict[option_service] = {'amount': option_service_dict[option_service], 'price' : price_option_service}

            id_price = get_room_by_code_random[10]
            id_duration = get_room_by_code_random[9]

            query_get_price = select(price).where(price.c.id == id_price)
            query_get_duration = select(duration).where(duration.c.id == id_duration)

            get_price_room = conn.execute(query_get_price).fetchone()
            get_duration_room = conn.execute(query_get_duration).fetchone()

        return {'get_room_by_code_room' : get_room_by_code_random, 'price_room' : get_price_room,
            'lst_name_image_firebase' : get_room_by_code_random[13], 'get_duration_room' : get_duration_room,
            'default_service_dict' : default_service_dict, 'option_service_dict' : option_service_dict}
    except:
        return False

def get_valid_duration_room():
    try:
        with engine.connect() as conn:
            lst_valid_duration_room = []
            lst_price = []
            lst_name_image_firebase = []
            
            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()
            
            for each_room in get_all_room:
                id_duration_each_room = each_room.id_duration
                id_each_room = each_room.id
                
                query_each_duration = select(duration).where(duration.c.id == id_duration_each_room)
                each_duration = conn.execute(query_each_duration).fetchone()

                start_time_duration = each_duration.date_start_duration
                end_time_duration = each_duration.date_end_duration
                current_time = datetime.now()

                if start_time_duration < current_time < end_time_duration:
                    lst_valid_duration_room.append(each_room)
            
            for index in lst_valid_duration_room:
                id_price = index[10]
                query_get_price = select(price).where(price.c.id == id_price)
                get_each_price = conn.execute(query_get_price).fetchone()
                lst_price.append(get_each_price)
                lst_name_image_firebase.append(index[13])
                
    except:
        return False

# This function create a new booking room.
def post_new_booking_room(id_user_para, id_all_room_para, time_start_para, default_service_para, option_service_para, second_rent_para):
    try:
        with engine.connect() as conn:
            ins_new_booking_room = booking_room.insert().values(id_users = id_user_para, id_all_room = id_all_room_para,
                default_service = default_service_para, option_service = option_service_para, time_booking = datetime.now(),
                time_start = time_start_para, second_rent = second_rent_para)
            
            conn.execute(ins_new_booking_room)
            check_current_os(conn)
        return True
    except Exception as e:
        print(e)
        return False
