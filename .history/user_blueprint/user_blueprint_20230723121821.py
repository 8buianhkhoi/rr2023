from flask import Blueprint, render_template, session, redirect, url_for

from . import exec_data

user_blueprint = Blueprint('USER_bp', __name__, static_folder = 'static', template_folder = 'templates')

@user_blueprint.route('/homepage-user')
def homepage_user_page():
    return_para = {}
    get_all_room = exec_data.get_all_room()

    if get_all_room != False:
        return_para['all_room'] = get_all_room['all_room']

    return render_template('homepage_user_page.html',**return_para)

@user_blueprint.route('/log-out-user')
def log_out_user():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))