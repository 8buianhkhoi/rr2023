from flask import Blueprint, render_template, session, redirect, url_for
from firebase_admin import storage
import ast
import base64

from . import exec_data

user_blueprint = Blueprint('USER_bp', __name__, static_folder = 'static', template_folder = 'templates')

def download_img_from_firebase(remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    image_data = blob.download_as_bytes()
    base64_img = base64.b64encode(image_data).decode()
    return base64_img

@user_blueprint.route('/log-out-user')
def log_out_user():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))
 
@user_blueprint.route('/homepage-user/type/<type_room>')
def type_room_homepage(type_room = None):
    return_para = {}
    lst_base64_image_firebase = []

    if type_room == 'big':
        get_type_room = exec_data.get_type_room(type_room_para = 'big')
    elif type_room == 'medium':
        get_type_room = exec_data.get_type_room(type_room_para = 'medium')
    elif type_room == 'small':
        get_type_room = exec_data.get_type_room(type_room_para = 'small')
    elif type_room == 'all':
        get_type_room = exec_data.get_type_room(type_room_para = 'all')
    else:
        return "Đường dẫn sai rồi nè :v"
    
    if get_type_room != False:
        return_para['get_type_room'] = get_type_room['get_type_room']
        return_para['lst_price'] = get_type_room['lst_price']

    # for each_image_firebase in get_type_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('type_room_homepage.html', **return_para)

@user_blueprint.route('/homepage-user/addr/<province_para>')
def province_room_homepage(province_para):
    return_para = {}
    lst_base64_image_firebase = []
    get_province_room = exec_data.get_room_by_province(province_para)

    if get_province_room != False:
        return_para['province_room'] = get_province_room['get_room_by_province']
        return_para['lst_price'] = get_province_room['lst_price']

    # for each_image_firebase in get_province_room['lst_name_image_firebase']:
    #     dict_image_temp = ast.literal_eval(each_image_firebase)
    #     for each_image in dict_image_temp:
    #         base64_str = download_img_from_firebase(dict_image_temp[each_image])
    #         dict_image_temp[each_image] = base64_str
    #     lst_base64_image_firebase.append(dict_image_temp)

    # return_para['lst_base64_image_firebase'] = lst_base64_image_firebase

    return render_template('province_room_homepage.html', **return_para)


# This route show information about the room, show detail and all information about one room
# each room have each code random separate, so we use code_random for get information from database
@user_blueprint.route('/details-room/<code_room>')
def detail_room(code_room):
    return_para = {}

    get_room_by_code_room = exec_data.get_detail_room_by_code_room(code_room)

    if get_room_by_code_room != False:
        return_para['get_room_by_code_room'] = get_room_by_code_room['get_room_by_code_room']
    print(get_room_by_code_room['get_room_by_code_room'])
    return render_template('detail_room.html', **return_para)
    