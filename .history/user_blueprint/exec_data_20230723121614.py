from sqlalchemy import select

from models.models import *

def get_all_room():
    try:
        with engine.connect() as conn:
            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()
        return {'all_room' : get_all_room}
    except:
        return False

    