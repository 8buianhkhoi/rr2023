from flask import Blueprint, render_template
from sqlalchemy import select

sign_up_blueprint = Blueprint('SU_bp', __name__, static_folder = 'static', template_folder = 'templates')

@sign_up_blueprint.route('/', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        # Because each user have different username, name, tel and gmail
        # So we need to check it, if it exits in database throw error
        # I use rowcount property to count the row if where statement
        # if return 0 it mean not exits

        # Name display and username is different. username just for login
        # Create 1 dictionary to store error duplicate
        err_check_duplicate = {}

        get_username = request.form['username_signup']
        get_tel = request.form['tel_signup']
        get_gmail = request.form['gmail_signup']
        get_name = request.form['name_signup']

        query_duplicate_username = conn.execute(select(users).where(users.c.user_name == get_username)).rowcount
        query_duplicate_tel = conn.execute(select(users).where(users.c.tel == get_tel)).rowcount
        query_duplicate_gmail = conn.execute(select(users).where(user.c.gmail == get_gmail)).rowcount
        query_duplicate_name = conn.execute(select(users).where(user.c.name == get_name)).rowcount

    return render_template('sign_up.html')