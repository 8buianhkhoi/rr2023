from sqlalchemy import select

from models.models import users, engine

def check_duplicate_signup(username_para, tel_para, gmail_para, name_para):
    err_check_duplicate = {}

    with engine.connect() as conn:
        print(username_para)
        query_duplicate_username = conn.execute(select(users).where(users.c.user_name == username_para)).rowcount
        print(query_duplicate_username)
        query_duplicate_tel = conn.execute(select(users).where(users.c.tel == tel_para)).rowcount
        query_duplicate_gmail = conn.execute(select(users).where(users.c.gmail == gmail_para)).rowcount
        query_duplicate_name = conn.execute(select(users).where(users.c.full_name == name_para)).rowcount

        if query_duplicate_username != 0:
            err_check_duplicate[username_para] = f'Duplicate user name : {username_para}'
        if query_duplicate_tel != 0:
            err_check_duplicate[tel_para] = f'Duplicate telephone number : {tel_para}'
        if query_duplicate_gmail != 0:
            err_check_duplicate[gmail_para] = f'Duplicate Gmail Address : {gmail_para}'
        if query_duplicate_name != 0:
            err_check_duplicate[name_para] = f'Duplicate name : {name_para}'
    
    if len(err_check_duplicate) == 0:
        return True 
    else:
        return err_check_duplicate

def ins_new_user(username_para, name_para, pass_para, gmail_para, tel_para, gender_para):
    try:
        with engine.connect() as conn:
            query_ins = users.insert().values(user_name = username_para, password = pass_para, full_name = name_para,
                tel = tel_para, gmail = gmail_para, status = 'OK', gender = gender_para)
            conn.execute(query_ins)
        return True
    except:
        return False
