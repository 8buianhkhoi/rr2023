from flask import Blueprint, render_template


from . import exec_data

sign_up_blueprint = Blueprint('SU_bp', __name__, static_folder = 'static', template_folder = 'templates')

@sign_up_blueprint.route('/', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        # Because each user have different username, name, tel and gmail
        # So we need to check it, if it exits in database throw error
        # I use rowcount property to count the row if where statement
        # if return 0 it mean not exits

        # Name display and username is different. username just for login
        # Create 1 dictionary to store error duplicate
        get_username = request.form['username_signup']
        get_tel = request.form['tel_signup']
        get_gmail = request.form['gmail_signup']
        get_name = request.form['name_signup']

        check_duplicate = exec_data.check_duplicate_signup(username_para = get_username, tel_para = get_tel, gmail_para = get_gmail, name_para = get_name)
        if check_duplicate != True:
            return render_template('sign_up.html')

    return render_template('sign_up.html')