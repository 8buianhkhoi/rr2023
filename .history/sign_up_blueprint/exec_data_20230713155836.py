from sqlalchemy import select

from models.models import users

def check_duplicate_signup(username_para, tel_para, gmail_para, name_para):
    with engine.connect() as conn:
        err_check_duplicate = {}

        query_duplicate_username = conn.execute(select(users).where(users.c.user_name == username_para)).rowcount
        query_duplicate_tel = conn.execute(select(users).where(users.c.tel == tel_para)).rowcount
        query_duplicate_gmail = conn.execute(select(users).where(user.c.gmail == gmail_para)).rowcount
        query_duplicate_name = conn.execute(select(users).where(user.c.full_name == name_para)).rowcount

        if query_duplicate_username != 0:
            err_check_duplicate[username_para] = f'Duplicate user name : {username_para}'
        if query_duplicate_tel != 0:
            err_check_duplicate[tel_para] = f'Duplicate telephone number : {tel_para}'