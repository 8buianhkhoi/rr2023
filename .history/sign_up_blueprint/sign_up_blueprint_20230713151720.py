from flask import Blueprint, render_template
from sqlalchemy import select

sign_up_blueprint = Blueprint('SU_bp', __name__, static_folder = 'static', template_folder = 'templates')

@sign_up_blueprint.route('/', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        get_username = request.form['username_signup']
        get_tel = request.form['tel_signup']
        get_gmail = request.form['gmail_signup']

        query_duplicate_username = select(users).where(users.c.user_name == get_username)
    return render_template('sign_up.html')