from flask import Blueprint, render_template

sign_up_blueprint = Blueprint('SU_bp', __name__, static_folder = 'static', template_folder = 'templates')