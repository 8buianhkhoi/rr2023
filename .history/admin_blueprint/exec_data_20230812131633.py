from sqlalchemy import select, and_
import ast
from datetime import datetime
import platform

from models.models import *

from . import get_pagination

limit_show_per_page = 30

# This function check OS. Linux os need to do something more than Windows and Darwin ( MacOS )
# In this function if linux os, need to add commit function for execute database ( insert, delete, update)
def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

def get_all_users(current_page_para):
    try:
        with engine.connect() as conn:
            query_get_all_user = select(users).where(users.c.type_user == 'user')
            length_all_user = conn.execute(query_get_all_user).rowcount 

            get_pagination_page = get_pagination.get_pagination(current_page = current_page_para, limit_show = limit_show_per_page, length_all_order = length_all_user)

            get_all_user = conn.execute(query_get_all_user.offset(get_pagination_page[1]).limit(limit_show_per_page)).fetchall()
        return {'get_all_user' : get_all_user}
    except:
        return False

def get_all_editor(current_page_para):
    try:
        with engine.connect() as conn:
            query_get_all_editor = select(users).where(users.c.type_user == 'editor')
            length_all_editor = conn.execute(query_get_all_editor).rowcount 

            get_pagination_page = get_pagination.get_pagination(current_page = current_page_para, limit_show = limit_show_per_page, length_all_order = length_all_editor)

            get_all_editor = conn.execute(query_get_all_editor.offset(get_pagination_page[1]).limit(limit_show_per_page)).fetchall()
        return {'get_all_editor' : get_all_editor}
    except:
        return False

def post_del_user(user_id_para):
    try:
        with engine.connect() as conn:
            del_cmd = users.delete().where(users.c.id == user_id_para)
            conn.execute(del_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def get_user_by_id(id_user_para):
    try:
        with engine.connect() as conn:
            query_get_user_by_id = select(users).where(users.c.id == id_user_para)
            get_user_by_id = conn.execute(query_get_user_by_id).fetchone()
            
        return {'get_user_by_id' : get_user_by_id}
    except:
        return False

def post_edit_user_by_id(id_user_para, gmail_user_para, tel_user_para):
    try:
        with engine.connect() as conn:
            query_update_user_by_id = users.update().where(users.c.id == id_user_para).values(tel = tel_user_para, gmail = gmail_user_para)
            conn.execute(query_update_user_by_id)
            check_current_os(conn)
        return True
    except:
        return False

def post_del_editor(post_id_editor_para):
    try:
        with engine.connect() as conn:
            del_cmd = users.delete().where(users.c.id == post_id_editor_para)
            conn.execute(del_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def post_edit_editor_by_id(id_editor_para, gmail_editor_para, tel_editor_para):
    try:
        with engine.connect() as conn:
            query_update_editor_by_id = users.update().where(users.c.id == id_editor_para).values(tel = tel_editor_para, gmail = gmail_editor_para)
            conn.execute(query_update_editor_by_id)
            check_current_os(conn)
        return True
    except:
        return False

def get_editor_by_id(id_editor_para):
    try:
        with engine.connect() as conn:
            query_get_editor_by_id = select(users).where(users.c.id == id_editor_para)
            get_user_by_id = conn.execute(query_get_user_by_id).fetchone()
            
        return {'get_user_by_id' : get_user_by_id}
    except:
        return False