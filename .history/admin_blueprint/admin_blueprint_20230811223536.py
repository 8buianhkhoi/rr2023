from flask import Blueprint, render_template, session, redirect, url_for

from . import exec_data

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')

@admin_blueprint.route('/homepage-admin')
def homepage_admin():

    return render_template('homepage_admin.html')

@admin_blueprint.route('/log-out-admin')
def log_out_admin():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@admin_blueprint.route('/all-users/<int:current_page>', methods = ['GET', 'POST'])
def show_all_users(current_page):
    return_para = {}

    get_all_user = exec_data.get_all_users(current_page_para = current_page)

    if get_all_user != False:
        return_para['get_all_user'] = get_all_user['get_all_user']

    return render_template('admin_show_all_user.html', **return_para)