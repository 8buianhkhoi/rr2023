from flask import Blueprint, render_template, session

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')

@admin_blueprint.route('/homepage-admin')
def homepage_admin():

    return render_template('homepage_admin.html')

@admin_blueprint.route('/log-out-admin')
def log_out_admin():
