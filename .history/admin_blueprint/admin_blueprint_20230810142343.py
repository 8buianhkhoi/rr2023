from flask import Blueprint

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')