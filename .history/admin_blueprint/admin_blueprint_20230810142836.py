from flask import Blueprint

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')

@admin_blueprint.route('/homepage-admin')
def homepage_admin():

    return render_template('homepage_admin.html')