from flask import Blueprint, render_template, session, redirect, url_for, request

from . import exec_data

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')

@admin_blueprint.route('/homepage-admin')
def homepage_admin():

    return render_template('homepage_admin.html')

@admin_blueprint.route('/log-out-admin')
def log_out_admin():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@admin_blueprint.route('/all-users/show-all/<int:current_page>', methods = ['GET', 'POST'])
def show_all_users(current_page):
    if request.method == 'POST':
        if 'submit_delete_user' in request.form:
            post_id_user = request.form['value_delete_user']
            exec_del_user_by_id = exec_data.post_del_user(post_id_user)

            if exec_del_user_by_id == True:
                return redirect(url_for('ADMIN_bp.show_all_users', current_page = 1, msg_del_user_by_id_success = 'True'))
            else:
                return redirect(url_for('ADMIN_bp.show_all_users', current_page = 1, msg_del_user_by_id_fail = 'True'))

        elif 'submit_edit_user' in request.form:
            post_id_user = request.form['value_edit_user']
            return redirect(url_for('ADMIN_bp.edit_user_by_user_name', user_id_url = post_id_user))

    return_para = {}

    msg_del_user_by_id_success = request.args.get('msg_del_user_by_id_success')
    msg_del_user_by_id_fail = request.args.get('msg_del_user_by_id_fail')
    msg_edit_user_by_id_success = request.args.get('msg_edit_user_by_id_success')
    msg_edit_user_by_id_fail = request.args.get('msg_edit_user_by_id_fail')

    if msg_del_user_by_id_success is not None:
        return_para['msg_del_user_by_id_success'] = 'Xóa thành công'
    if msg_del_user_by_id_fail is not None:
        return_para['msg_del_user_by_id_fail'] = 'Xóa thất bại'
    if msg_edit_user_by_id_success is not None:
        return_para['msg_edit_user_by_id_success'] = 'Chỉnh sửa thành công'
    if msg_edit_user_by_id_fail is not None:
        return_para['msg_edit_user_by_id_fail'] = 'Chỉnh sửa thất bại'
        
    get_all_user = exec_data.get_all_users(current_page_para = current_page)

    if get_all_user != False:
        return_para['get_all_user'] = get_all_user['get_all_user']

    return render_template('admin_show_all_user.html', **return_para)

@admin_blueprint.route('/all-users/edit-user/<user_id_url>', methods = ['GET', 'POST'])
def edit_user_by_user_name(user_id_url):
    if request.method == 'POST':
        if 'submit_edit_user' in request.form:
            tel_edit_user = request.form['input_tel_user']
            gmail_edit_user = request.form['input_gmail_user']

            exec_update_user = exec_data.post_edit_user_by_id(id_user_para = user_id_url, gmail_user_para = gmail_edit_user, tel_user_para = tel_edit_user)

            if exec_update_user == True:
                return redirect(url_for('ADMIN_bp.show_all_users', current_page = 1, msg_edit_user_by_id_success = 'True'))
            else:
                return redirect(url_for('ADMIN_bp.show_all_users', current_page = 1, msg_edit_user_by_id_fail = 'True'))

    return_para = {}

    get_user_by_id = exec_data.get_user_by_id(user_id_url)

    if get_user_by_id != False:
        return_para['get_user_by_id'] = get_user_by_id['get_user_by_id']

    return render_template('edit_user_name.html', **return_para)

@admin_blueprint.route('/all-editor/show_all/<int:current_page_url>', methods = ['GET', 'POST'])
def show_all_editor(current_page_url):
    if request.method == 'POST':
        if 'submit_delete_editor' in request.form:
        
        elif 'submit_edit_editor' in request.form:
            pass
            
    return_para = {}

    get_all_editor = exec_data.get_all_editor(current_page_url)

    if get_all_editor != False:
        return_para['get_all_editor'] = get_all_editor['get_all_editor']

    return render_template('show_all_editor.html', **return_para)