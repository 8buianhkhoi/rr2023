import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
import base64
import io 

def bar_chart_count_status_booking_room(count_pending_status_para, count_cancel_status_para, count_finish_status_para):
    buffer = io.BytesIO()
    plt.figure()
    
    lst_label_plot = ['Pending', 'Cancel', 'Finish']

    plt.bar(lst_label_plot, [count_pending_status_para, count_cancel_status_para, count_finish_status_para])
    plt.xlabel('Status')
    plt.ylabel('Count')
    plt.title('Count status booking room')
    plt.legend()

    buffer.seek(0)
    plt.show()
    plt.close()

    bar_chart_base64 = base64.b64encode(buffer.getvalue()).decode()

    return bar_chart_base64
