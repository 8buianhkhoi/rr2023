import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
import base64
import io 

def bar_chart_count_status_booking_room(count_pending_status_para, count_cancel_status_para, count_finish_status_para):
    buffer = io.BytesIO()
    plt.figure()

    lst_label_plot = ['Pending', 'Cancel', 'Finish']
    
    plt.bar()