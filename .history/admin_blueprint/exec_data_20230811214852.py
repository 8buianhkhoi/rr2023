from sqlalchemy import select, and_
import ast
from datetime import datetime
import platform

from models.models import *

from . import get_pagination

limit_show_per_page = 30

# This function check OS. Linux os need to do something more than Windows and Darwin ( MacOS )
# In this function if linux os, need to add commit function for execute database ( insert, delete, update)
def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

def get_all_users():
    try:
        with engine.connect() as conn:
            query_get_all_user = select(users).where(and_(users.c.type_user == 'user', users.c.status == 'OK'))
            length_all_user = conn.execute(query_get_all_user).rowcount 
            
            get_all_user = conn.execute(query_get_all_user).fetchall()
    except:
        return False