from sqlalchemy import select, and_
import ast
from datetime import datetime
import platform

from models.models import *

from . import get_pagination

limit_show_per_page = 30

# This function check OS. Linux os need to do something more than Windows and Darwin ( MacOS )
# In this function if linux os, need to add commit function for execute database ( insert, delete, update)
def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

def get_all_users(current_page_para):
    try:
        with engine.connect() as conn:
            query_get_all_user = select(users).where(users.c.type_user == 'user')
            length_all_user = conn.execute(query_get_all_user).rowcount 

            get_pagination_page = get_pagination.get_pagination(current_page = current_page_para, limit_show = limit_show_per_page, length_all_order = length_all_user)

            get_all_user = conn.execute(query_get_all_user.offset(get_pagination_page[1]).limit(limit_show_per_page)).fetchall()
        return {'get_all_user' : get_all_user}
    except:
        return False