from flask import Blueprint, render_template, session, redirect, url_for, request

from . import exec_data

admin_blueprint = Blueprint('ADMIN_bp', __name__, static_folder = 'static', template_folder = 'templates')

@admin_blueprint.route('/homepage-admin')
def homepage_admin():

    return render_template('homepage_admin.html')

@admin_blueprint.route('/log-out-admin')
def log_out_admin():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@admin_blueprint.route('/all-users/<int:current_page>', methods = ['GET', 'POST'])
def show_all_users(current_page):
    if request.method == 'POST':
        if 'submit_delete_user' in request.form:
            post_id_user = request.form['value_delete_user']
            exec_del_user_by_id = exec_data.post_del_user(post_id_user)

            if exec_del_user_by_id == True:
                return redirect(url_for('ADMIN_bp.show_all_users', current_page = 1, msg_del_user_by_id_success = 'True'))

        elif 'submit_edit_user' in request.form:
            pass

    return_para = {}

    get_all_user = exec_data.get_all_users(current_page_para = current_page)

    if get_all_user != False:
        return_para['get_all_user'] = get_all_user['get_all_user']

    return render_template('admin_show_all_user.html', **return_para)