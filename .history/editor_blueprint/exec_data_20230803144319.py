from sqlalchemy import select
import platform

from models.models import *

from . import get_pagination

limit_show_per_page = 30

def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

# Get method

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return {'all_price' : all_price}
    except:
        return False

def get_all_duration():
    try:
        with engine.connect() as conn:
            query_select = select(duration)
            all_duration = conn.execute(query_select).fetchall()
        return {'all_duration' : all_duration}
    except:
        return False

def get_all_service():
    try:
        with engine.connect() as conn:
            query_default_service = select(default_service_room)
            query_option_service = select(option_service_room)

            all_default_service = conn.execute(query_default_service).fetchall()
            all_option_service = conn.execute(query_option_service).fetchall()

        return {'all_default_service' : all_default_service, 'all_option_service' : all_option_service}
    except:
        return False

def get_all_booking_room():
    try:
        with engine.connect() as conn:
            query_get_all_booking_room = select(booking_room)
            all_booking_room = conn.execute(query_get_all_booking_room).fetchall()
            
    except:
        return False

# post method

def post_new_price(new_price_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = price.insert().values(price = new_price_para, date_start_price = new_start_time_para,
                date_end_price = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def post_new_duration(new_duration_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = duration.insert().values(duration = new_duration_para, date_start_duration = new_start_time_para,
                date_end_duration = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False
    
def post_new_default_service(new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = default_service_room.insert().values(name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False
    
def post_new_option_service(new_name_para, new_id_para):
    try:
        with engine.connect() as conn:
            ins_cmd = option_service_room.insert().values(name = new_name_para, id_price = new_id_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def post_new_room(code_room_para, name_room_para, capacity_para, square_para, type_para, province_para, district_para, 
        ward_para, id_duration_para, id_price_para, default_service_para, option_service_para):
    try:
        with engine.connect() as conn:
            ins_new_room = all_room.insert().values(code_room = code_room_para, name_room = name_room_para, 
                capacity = capacity_para, square = square_para, type = type_para, province = province_para, district = district_para,
                ward = ward_para, id_duration = id_duration_para, id_price = id_price_para, default_service = default_service_para,
                option_service = option_service_para)
            conn.execute(ins_new_room)
            check_current_os(conn)
        return True
    except:
        return False

def get_booking_room(status_booking_para, current_page_para):
    try:
        with engine.connect() as conn:
            lst_all_room = []
            lst_all_price = []

            if status_booking_para == 'all':
                query_get_booking_room = select(booking_room)
            elif status_booking_para = 'cancel':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Cancel')
            elif status_booking_para = 'finish':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Finish')
            elif status_booking_para = 'pending':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Pending')

            length_query_get_booking_room = conn.execute(query_get_booking_room).rowcount
            get_pagination_page = get_pagination.get_pagination(current_page_para, limit_show_per_page, length_query_get_booking_room)

            get_booking_room = conn.execute(query_get_booking_room.offset(get_pagination_page[1]).limit(limit_show_per_page).order_by(booking_room.c.time_booking.desc())).fetchall()
    
            for each_booking in get_booking_room:
                query_get_room = select(all_room).where(all_room.c.id == each_booking.id_all_room)
                lst_all_room.append(conn.execute(query_get_room).fetchone())
                id_price_temp = conn.execute(query_get_room).fetchone().id_price
                query_get_price_room = select(price).where(price.c.id == id_price_temp)
                lst_all_price.append(conn.execute(query_get_price_room).fetchone())
    except:
        return False
