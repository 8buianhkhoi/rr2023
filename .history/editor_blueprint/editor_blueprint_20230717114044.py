from flask import Blueprint, render_template, session, redirect, url_for, request

from . import exec_data

editor_blueprint = Blueprint('EDITOR_bp', __name__, static_folder = 'static', template_folder = 'templates')

@editor_blueprint.route('/')
def homepage_editor_page():
    return render_template('homepage_editor_page.html')

@editor_blueprint.route('/insert-new-room')
def ins_new_room():
    all_duration = exec_data.get_all_duration()['all_duration']
    all_price = exec_data.get_all_price()['all_price']

    return_para = {}
    if all_duration != False:
        return_para['all_duration'] = all_duration
    if all_price != False:
        return_para['all_price'] = all_price

    return render_template('ins_new_room.html', **return_para)

@editor_blueprint.route('/log-out')
def log_out():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@editor_blueprint.route('/price-and-duration')
def price_and_duration():
    return render_template('price_and_duration.html')

@editor_blueprint.route('/price', methods = ['GET', 'POST'])
def price_editor_page():
    if request.method == 'POST':
        if 'submit_ins_new_price' in request.form:
            post_new_price = request.form['new_price']
            post_new_start_time = request.form['new_time_start']
            post_new_end_time = request.form['new_time_end']
            post_new_name = request.form['new_name']

            # Because end time can accept two values. First is datetime like 2023-12-12 12:12:12
            # Second is empty string. if empty string it mean editor not input end time so end time is infinite
            if len(post_new_end_time) == 0:
                post_new_end_time = None
            
            # Insert new price.
            exec_ins_database = exec_data.post_new_price(new_price_para = post_new_price,
                new_start_time_para = post_new_start_time, new_end_time_para = post_new_end_time, new_name_para = post_new_name)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.price_editor_page', ins_new_price_msg_success = 'Tạo giá mới thành công'))
            else:
                return redirect(url_for('EDITOR_bp.price_editor_page', ins_new_price_msg_fail = 'Tạo giá mới thất bại'))

    return_para = {}
    get_price_database = exec_data.get_all_price()

    ins_new_price_msg_success = request.args.get('ins_new_price_msg_success')
    ins_new_price_msg_fail = request.args.get('ins_new_price_msg_fail')

    # When msg is success background color is green, fail is red. So we need create two different variable for check
    # After return to templates. Template can easy css and understand
    if ins_new_price_msg_success is not None:
        return_para['ins_new_price_msg_success'] = ins_new_price_msg_success
    if ins_new_price_msg_fail is not None:
        return_para['ins_new_price_msg_fail'] = ins_new_price_msg_fail

    if get_price_database == False:
        return_para['err_msg'] = "Have some problem. Can't load all price"
    elif type(get_price_database['all_price']) is list:
        return_para['all_price'] = get_price_database['all_price']

    return render_template('price_editor_page.html', **return_para)


@editor_blueprint.route('/duration', methods = ['GET', 'POST'])
def duration_editor_page():
    if request.method == 'POST':
        if 'submit_ins_new_duration' in request.form:
            post_new_duration = request.form['new_duration']
            post_new_start_time = request.form['new_start_time']
            post_new_end_time = request.form['new_end_time']
            post_new_duration_name = request.form['new_name']

            # Because end time can accept two values. First is datetime like 2023-12-12 12:12:12
            # Second is empty string. if empty string it mean editor not input end time so end time is infinite
            if len(post_new_end_time) == 0:
                post_new_end_time = None

            # Insert new duration.
            exec_ins_database = exec_data.post_new_duration(new_duration_para = post_new_duration,
                new_start_time_para = post_new_start_time, new_end_time_para = post_new_end_time, new_name_para = post_new_duration_name)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.duration_editor_page', ins_new_duration_msg_success = 'Tạo giá mới thành công'))
            else:
                return redirect(url_for('EDITOR_bp.duration_editor_page', ins_new_duration_msg_fail = 'Tạo giá mới thất bại'))
    
    return_para = {}
    get_duration_database = exec_data.get_all_duration()

    ins_new_duration_msg_success = request.args.get('ins_new_duration_msg_success')
    ins_new_duration_msg_fail = request.args.get('ins_new_duration_msg_fail')
    
    # When msg is success background color is green, fail is red. So we need create two different variable for check
    # After return to templates. Template can easy css and understand
    if ins_new_duration_msg_success is not None:
        return_para['ins_new_duration_msg_success'] = ins_new_duration_msg_success
    if ins_new_duration_msg_fail is not None:
        return_para['ins_new_duration_msg_fail'] = ins_new_duration_msg_fail

    if get_duration_database == False:
        return_para['err_msg'] = "Have some problem. Can't load all duration"
    elif type(get_duration_database['all_duration']) is list:
        return_para['all_duration'] = get_duration_database['all_duration']

    return render_template('duration_editor_page.html', **return_para)

@editor_blueprint.route('/service', methods = ['GET', 'POST'])
def service_editor_page():
    return render_template('service_editor_page.html')

@editor_blueprint.route('/service/defaults', methods = ['GET', 'POST'])
def default_service_editor_page():
    all_price = exec_data.get_all_price()
    return_para = {}

    if all_price != False:
        return_para['all_price'] = all_price['all_price']

    return render_template('default_service_editor_page.html')
