from sqlalchemy import select

from models.models import *

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return all_price
    except:
        return False

