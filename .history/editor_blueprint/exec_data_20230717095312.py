from sqlalchemy import select

from models.models import *

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return {'all_price' : all_price}
    except:
        return False

def get_all_duration():
    try:
        with engine.connect() as conn:
            query_select = select(duration)
            all_duration = conn.execute(query_select).fetchall()
        return {'all_duration' : all_duration}
    except:
        return False

def post_new_price(new_price_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = price.insert().values(price = new_price_para, date_start_price = new_start_time_para,
                date_end_price = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
        return True
    except:
        return False

def post_new_duration(new_duration_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = duration.insert().values(duration = new_duration_para, date_start_duration = new_start_time_para,
                date_end_duration = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
        return True
    except Exception as e:
        return False