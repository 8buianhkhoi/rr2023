from sqlalchemy import select

from models.models import *

# Get method

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return {'all_price' : all_price}
    except:
        return False

def get_all_duration():
    try:
        with engine.connect() as conn:
            query_select = select(duration)
            all_duration = conn.execute(query_select).fetchall()
        return {'all_duration' : all_duration}
    except:
        return False

def get_all_service():
    try:
        with engine.connect() as conn:
            query_default_service = select(default_service_room)
            query_option_service = select(option_service_room)

            all_default_service = conn.execute(query_default_service).fetchall()
            all_option_service = conn.execute(query_option_service).fetchall()

        return {'all_default_service' : all_default_service, 'all_option_service' : all_option_service}
    except:
        return False

# post method

def post_new_price(new_price_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = price.insert().values(price = new_price_para, date_start_price = new_start_time_para,
                date_end_price = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
        return True
    except:
        return False

def post_new_duration(new_duration_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = duration.insert().values(duration = new_duration_para, date_start_duration = new_start_time_para,
                date_end_duration = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
        return True
    except:
        return False
    
def post_new_default_service(new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = default_service_room.insert().values(name = new_name_para)
            conn.execute(ins_cmd)
        return True
    except:
        return False
    
def post_new_option_service(new_name_para, new_id_para):
    try:
        with engine.connect() as conn:
            ins_cmd = option_service_room.insert().values(name = new_name_para, id_price = new_id_para)
            conn.execute(ins_cmd)
        return True
    except:
        return False

def post_new_room(code_room_para, name_room_para, capacity_para, square_para, type_para, province_para, district_para, 
        ward_para, id_duration_para, id_price_para, default_service_para, option_service_para):
    try:
        with engine.connect() as conn:
            ins_cmd = all_room.insert().values(code_room = code_room_para, name_room = name_room_para, 
                capacity = capacity_para, square = square_para, type = type_para, province = province_para, district = district_para,
                ward = ward_para, id_duration = id_duration_para, id_price = id_price_para, default_service = default_service_para,
                option_service = option_service_para)
            conn.execute(ins_cmd)
        return True
    except Exception as e:
        print(e)
        return False
