from sqlalchemy import select
import platform
from datetime import datetime

from models.models import *

from . import get_pagination

limit_show_per_page = 30

def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

# Get method

# This function have input is a list of room. Output is a list price match with list of room
def get_price_lst_room(lst_room_para):
    lst_price = []

    with engine.connect() as conn:
        for index in lst_room_para:
            id_price = index[10]
            query_get_price = select(price).where(price.c.id == id_price)
            get_each_price = conn.execute(query_get_price).fetchone()
            lst_price.append(get_each_price)
    
    return lst_price

# This function try to get name image of firebase store in all_room table
def get_img_name_firebase(lst_room_para):
    lst_name_image_firebase = []
    
    for index in lst_room_para:
        lst_name_image_firebase.append(index[13])
    
    return lst_name_image_firebase

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return {'all_price' : all_price}
    except:
        return False

def get_all_duration():
    try:
        with engine.connect() as conn:
            query_select = select(duration)
            all_duration = conn.execute(query_select).fetchall()
        return {'all_duration' : all_duration}
    except:
        return False

def get_all_service():
    try:
        with engine.connect() as conn:
            query_default_service = select(default_service_room)
            query_option_service = select(option_service_room)

            all_default_service = conn.execute(query_default_service).fetchall()
            all_option_service = conn.execute(query_option_service).fetchall()

        return {'all_default_service' : all_default_service, 'all_option_service' : all_option_service}
    except:
        return False

# def get_all_booking_room():
#     try:
#         with engine.connect() as conn:
#             query_get_all_booking_room = select(booking_room)
#             all_booking_room = conn.execute(query_get_all_booking_room).fetchall()
            
#     except:
#         return False

# post method

def post_new_price(new_price_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = price.insert().values(price = new_price_para, date_start_price = new_start_time_para,
                date_end_price = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def post_new_duration(new_duration_para, new_start_time_para, new_end_time_para, new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = duration.insert().values(duration = new_duration_para, date_start_duration = new_start_time_para,
                date_end_duration = new_end_time_para, full_name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False
    
def post_new_default_service(new_name_para):
    try:
        with engine.connect() as conn:
            ins_cmd = default_service_room.insert().values(name = new_name_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False
    
def post_new_option_service(new_name_para, new_id_para):
    try:
        with engine.connect() as conn:
            ins_cmd = option_service_room.insert().values(name = new_name_para, id_price = new_id_para)
            conn.execute(ins_cmd)
            check_current_os(conn)
        return True
    except:
        return False

def post_new_room(code_room_para, name_room_para, capacity_para, square_para, type_para, province_para, district_para, 
        ward_para, id_duration_para, id_price_para, default_service_para, option_service_para):
    try:
        with engine.connect() as conn:
            ins_new_room = all_room.insert().values(code_room = code_room_para, name_room = name_room_para, 
                capacity = capacity_para, square = square_para, type = type_para, province = province_para, district = district_para,
                ward = ward_para, id_duration = id_duration_para, id_price = id_price_para, default_service = default_service_para,
                option_service = option_service_para)
            conn.execute(ins_new_room)
            check_current_os(conn)
        return True
    except:
        return False

def get_booking_room(status_booking_para, current_page_para):
    try:
        with engine.connect() as conn:
            lst_all_room = []
            lst_all_price = []
            lst_name_user = []

            if status_booking_para == 'all':
                query_get_booking_room = select(booking_room)
            elif status_booking_para == 'cancel':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Cancel')
            elif status_booking_para == 'finish':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Finish')
            elif status_booking_para == 'pending':
                query_get_booking_room = select(booking_room).where(booking_room.c.status == 'Pending')

            length_query_get_booking_room = conn.execute(query_get_booking_room).rowcount
            get_pagination_page = get_pagination.get_pagination(current_page_para, limit_show_per_page, length_query_get_booking_room)

            get_lst_booking_room = conn.execute(query_get_booking_room.offset(get_pagination_page[1]).limit(limit_show_per_page).order_by(booking_room.c.time_booking.desc())).fetchall()
    
            for each_booking in get_lst_booking_room:
                query_get_room = select(all_room).where(all_room.c.id == each_booking.id_all_room)
                get_each_room = conn.execute(query_get_room).fetchone()
                lst_all_room.append(get_each_room)

                id_price_temp = get_each_room.id_price
                query_get_price_room = select(price).where(price.c.id == id_price_temp)
                lst_all_price.append(conn.execute(query_get_price_room).fetchone())

                id_user_temp = each_booking.id_users 
                query_get_user = select(users).where(users.c.id == id_user_temp)
                get_each_user = conn.execute(query_get_user).fetchone()
                lst_name_user.append(get_each_user.user_name)

        return {'lst_booking_room' : get_lst_booking_room, 'lst_price' : lst_all_price, 'lst_all_room' : lst_all_room, 
            'lst_name_user' : lst_name_user, 'pagination_page' : get_pagination_page[0]}
    except:
        return False

def get_booking_room_by_code(code_booking_para):
    try:
        with engine.connect() as conn:
            query_get_booking_by_code = select(booking_room).where(booking_room.c.code_booking == code_booking_para)
            get_booking_room_by_code = conn.execute(query_get_booking_by_code).fetchone()

            query_get_room_by_code = select(all_room).where(all_room.c.id == get_booking_room_by_code.id_all_room)
            get_room_by_code = conn.execute(query_get_room_by_code).fetchone()
        
            get_price_room = get_price_lst_room([get_room_by_code])
            get_img_name_firebase_room = get_img_name_firebase([get_room_by_code])

        return {'get_booking_room_by_code' : get_booking_room_by_code, 'get_room_by_code' : get_room_by_code, 
                'get_price_room' : get_price_room, 'get_img_name_firebase_room' : get_img_name_firebase_room}
    except:
        return False

def post_status_booking_room_to_cancel(code_booking_room_para):
    try:
        with engine.connect() as conn:
            update_status_booking_room = booking_room.update().where(booking_room.c.code_booking == code_booking_room_para).values(status = 'Cancel')
            conn.execute(update_status_booking_room)
            check_current_os(conn)
        return True
    except:
        return False

def post_delete_booking_room(code_booking_room_para):
    try:
        with engine.connect() as conn:
            update_flag_booking_delete = booking_room.update().where(booking_room.c.code_booking == code_booking_room_para).values(flag_booking = 'Delete')
            conn.execute(update_flag_booking_delete)
            check_current_os(conn)
        return True
    except:
        return False

def get_duration_by_id(id_duration_para):
    try:
        with engine.connect() as conn:
            query_get_duration = select(duration).where(duration.c.id == id_duration_para)
            get_duration = conn.execute(query_get_duration).fetchone()
        return {'get_duration' : get_duration}
    except:
        return False
    
def post_edit_booking_room_by_code(code_booking_para, time_start_para, minute_rent_para, note_booking_para, billing_option_para):
    try:
        with engine.connect() as conn:
            values_update_dict = {'time_start' : time_start_para, 'minute_rent' : minute_rent_para, 
                    'note_booking' : note_booking_para, 'billing_option' : billing_option_para}
            update_edit_booking_room_by_code = booking_room.update().where(booking_room.c.code_booking == code_booking_para).values(**values_update_dict)
            conn.execute(update_edit_booking_room_by_code)
            check_current_os(conn)
        return True
    except:
        return False

def get_all_users():
    try:
        with engine.connect() as conn:
            query_get_all_user = select(users).where(users.c.type_user == 'user')
            get_all_user = conn.execute(query_get_all_user).fetchall()
        return {'get_all_user' : get_all_user}
    except:
        return False
    
def get_valid_room():
    try:
        with engine.connect() as conn:
            lst_valid_room = []

            query_get_all_room = select(all_room)
            get_all_room = conn.execute(query_get_all_room).fetchall()

            for each_room in get_all_room:
                query_get_duration = select(duration).where(duration.c.id == each_room.id_duration)
                get_duration = conn.execute(query_get_duration).fetchone()

                current_time = datetime.now()

                if get_duration.date_start_duration <= current_time <= get_duration.date_end_duration:
                    lst_valid_room.append(each_room)
        return {'lst_valid_room' : lst_valid_room}
    except:
        return False
    
def get_room_by_code(code_room_para):
    try:
        with engine.connect() as conn:
            query_get_room_by_code = select(all_room).where(all_room.c.code_room == code_room_para)
            get_room_by_code_room = conn.execute(query_get_room_by_code).fetchone()
        return {'get_room_by_code_room' : get_room_by_code_room}
    except:
        return False

def get_price_dict_option_service(dict_option_service_para):
    try:
        with engine.connect() as conn:
            dict_price_option_service = {}

            for each_option_service in dict_option_service_para:
                query_get_id_price = select(option_service_room).where(option_service_room.c.name == each_option_service)
                get_id_price = conn.execute(query_get_id_price).fetchone().id_price
                
                query_get_price = select(price).where(price.c.id == get_id_price)
                get_price_each_option_service = conn.execute(query_get_price).fetchone().price

                dict_price_option_service[each_option_service] = {'amount' : dict_option_service_para[each_option_service], 'price' : get_price_each_option_service}
        return {'dict_price_option_service' : dict_price_option_service}
    except:
        return False

def get_price_by_id(id_price_para):
    try:
        with engine.connect() as conn:
            query_get_price_by_id = select(price).where(price.c.id == id_price_para)
            get_price_by_id = conn.execute(query_get_price_by_id).fetchone()
        return {'get_price_by_id' : get_price_by_id}
    except:
        return False

def get_id_room_by_code_room(code_room_para):
    try:
        with engine.connect() as conn:
            query_get_id_room = select(all_room).where(all_room.c.code_room == code_room_para)
            get_id_room = conn.execute(query_get_id_room).fetchone().id
        return {'get_id_room' : get_id_room}
    except:
        return False
    
def post_new_booking_room(id_user_para, id_all_room_para, time_start_para, default_service_para, option_service_para,
        minute_rent_para, total_price_para, option_service_price_para, note_booking_para, billing_option_para):
    try:
        with engine.connect() as conn:
            get_code_booking = datetime.now().strftime(f'%Y%m%d%H%M%S') + "_" + str(id_user_para) + "_"
            
            ins_new_booking_room = booking_room.insert().values(id_users = int(id_user_para), id_all_room = int(id_all_room_para),
                    time_booking = datetime.now(), time_start = time_start_para, default_service = default_service_para,
                    option_service = option_service_para, minute_rent = minute_rent_para, status='Pending', total_price = total_price_para,
                    option_service_price = option_service_price_para, note_booking = note_booking_para, code_booking = get_code_booking,
                    billing_option = billing_option_para, flag_booking = 'Live')
            
            conn.execute(ins_new_booking_room)
            check_current_os(conn)
        return True
    except:
        return False
    
def get_all_payment_method():
    try:
        with engine.connect() as conn:
            query_get_all_payment_method = select(payment_method)
            all_payment_method = conn.execute(query_get_all_payment_method).fetchall()
        return {'all_payment_method' : all_payment_method}
    except:
        return False

def delete_payment_method_by_name_method(name_payment_method_name_para):
    try:
        with engine.connect() as conn:
            delete_payment_method_cmd = payment_method.delete().where(payment_method.c.name == name_payment_method_name_para)
            conn.execute(delete_payment_method_cmd)
            check_current_os(conn)
        return True       
    except:
        return False

def update_information_payment_method(old_payment_method_name_para, new_payment_method_name_para, new_payment_method_note_para):
    try:
        with engine.connect() as conn:
            update_payment_method = payment_method.update().where(payment_method.c.name == old_payment_method_name_para).values(name = new_payment_method_name_para, note = new_payment_method_note_para)
            conn.execute(update_payment_method)
            check_current_os(conn)
        return True
    except:
        return False

def ins_new_payment_method(name_payment_method_para, note_payment_method_para):
    try:
        with engine.connect() as conn:
            ins_new_payment_method = payment_method.insert().values(name = name_payment_method_para, note = note_payment_method_para)
            conn.execute(ins_new_payment_method)
            check_current_os(conn)
        return True
    except:
        return False

def get_all_room(current_page_para):
    try:
        with engine.connect() as conn:
            query_get_all_room = select(all_room)
            length_all_room = conn.execute(query_get_all_room).rowcount

            get_pagination_page = get_pagination.get_pagination(current_page_para, limit_show_per_page, length_all_room)

            get_all_room = conn.execute(query_get_all_room.offset(get_pagination_page[1]).limit(limit_show_per_page)).fetchall()

            lst_price = get_price_lst_room(get_all_room)
            lst_img_name_firebase = get_img_name_firebase(get_all_room)

        return {'get_all_room' : get_all_room, 'lst_price' : lst_price, 'lst_img_name_firebase' : lst_img_name_firebase}
    except:
        return False