from flask import Blueprint, render_template, session, redirect, url_for, request
import ast 
import base64
from firebase_admin import storage
from PIL import Image 
from io import BytesIO
import datetime

from . import exec_data

editor_blueprint = Blueprint('EDITOR_bp', __name__, static_folder = 'static', template_folder = 'templates')

def uploadImageFirebase(base64_str, remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    img_bytes = base64.b64decode(base64_str)
    blob.upload_from_string(img_bytes, content_type = 'image/jpeg')

@editor_blueprint.route('/')
def homepage_editor_page():
    return render_template('homepage_editor_page.html')

@editor_blueprint.route('/insert-new-room', methods = ['GET', 'POST'])
def ins_new_room():
    if request.method == 'POST':
        if 'submit_new_room' in request.form:
            # Get input text from templates
            # using library ast and literal_eval function
            # When get value, some value like: "{'a':1, 'b':2}", type value is string, we want to change to dictionary
            # so we use literal_eval to change string to dictionary
            post_name_new_room = request.form['name_new_room']
            post_capacity_new_room = request.form['capacity_new_room']
            post_square_new_room =  request.form['square_new_room']
            post_type_new_room = request.form['type_new_room']
            post_province_new_room = request.form['province_new_room']
            post_district_new_room = request.form['district_new_room']
            post_ward_new_room = request.form['ward_new_room']
            post_duration_new_room = request.form['duration_new_room']
            post_price_new_room = request.form['price_new_room']
            post_default_service_new_room = ast.literal_eval(request.form['lst_default_service'])
            post_option_service_new_room = ast.literal_eval(request.form['lst_option_service'])
            post_all_image_new_room = ast.literal_eval(request.form['lst_all_image_new_room'])

            # Because post_name_new_room is unique for each room, sau we can combine with time to create a random code for room
            code_room_random = datetime.datetime.now().strftime(f'%Y%m%d%H%M%S') + "_" + post_name_new_room
            
            count_image_new_room = 1
            for each_image_new_room in post_all_image_new_room:
                # To display image by base64 string in html and by imt tag we need to use :
                # <img src='data:image/png;base64,.... so base64 after a commas so we need to index from 22 
                each_image_str_base64 = post_all_image_new_room[each_image_new_room][22:None]
                
                # Name of image we use 3 _ : code_room_random ___ count_image_new_room
                uploadImageFirebase(each_image_str_base64, f'{code_room_random}___{count_image_new_room}.png')
                count_image_new_room = count_image_new_room + 1
            
            exec_ins_new_room = exec_data.post_new_room(code_room_para = code_room_random, name_room_para = post_name_new_room,
                capacity_para = post_capacity_new_room, square_para = post_square_new_room, type_para = post_type_new_room,
                province_para = post_province_new_room, district_para = post_district_new_room, ward_para = post_ward_new_room,
                id_duration_para = post_duration_new_room, id_price_para = post_price_new_room, 
                default_service_para = str(post_default_service_new_room), option_service_para = str(post_option_service_new_room))
            
            if exec_ins_new_room == True:
                return redirect(url_for('EDITOR_bp.ins_new_room', msg_ins_new_room_success = "Tạo phòng mới thành công"))
            else:
                return redirect(url_for('EDITOR_bp.ins_new_room', msg_ins_new_room_fail = "Tạo phòng mới thất bại"))

    all_duration = exec_data.get_all_duration()
    all_price = exec_data.get_all_price()
    all_service = exec_data.get_all_service()

    return_para = {}
    if all_duration != False:
        return_para['all_duration'] = all_duration['all_duration']
    if all_price != False:
        return_para['all_price'] = all_price['all_price']
    if all_service != False:
        return_para['default_service'] = all_service['all_default_service']
        return_para['option_service'] = all_service['all_option_service']

    msg_ins_new_room_success = request.args.get('msg_ins_new_room_success')
    msg_ins_new_room_fail = request.args.get('msg_ins_new_room_fail')

    if msg_ins_new_room_success is not None:
        return_para['msg_ins_new_room_success'] = msg_ins_new_room_success
    if msg_ins_new_room_fail is not None:
        return_para['msg_ins_new_room_fail'] = msg_ins_new_room_fail
        
    return render_template('ins_new_room.html', **return_para)

@editor_blueprint.route('/log-out')
def log_out():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@editor_blueprint.route('/price-and-duration')
def price_and_duration():
    return render_template('price_and_duration.html')

@editor_blueprint.route('/price', methods = ['GET', 'POST'])
def price_editor_page():
    if request.method == 'POST':
        if 'submit_ins_new_price' in request.form:
            post_new_price = request.form['new_price']
            post_new_start_time = request.form['new_time_start']
            post_new_end_time = request.form['new_time_end']
            post_new_name = request.form['new_name']

            # Because end time can accept two values. First is datetime like 2023-12-12 12:12:12
            # Second is empty string. if empty string it mean editor not input end time so end time is infinite
            if len(post_new_end_time) == 0:
                post_new_end_time = None
            
            # Insert new price.
            exec_ins_database = exec_data.post_new_price(new_price_para = post_new_price,
                new_start_time_para = post_new_start_time, new_end_time_para = post_new_end_time, new_name_para = post_new_name)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.price_editor_page', ins_new_price_msg_success = 'Tạo giá mới thành công'))
            else:
                return redirect(url_for('EDITOR_bp.price_editor_page', ins_new_price_msg_fail = 'Tạo giá mới thất bại'))

    return_para = {}
    get_price_database = exec_data.get_all_price()

    ins_new_price_msg_success = request.args.get('ins_new_price_msg_success')
    ins_new_price_msg_fail = request.args.get('ins_new_price_msg_fail')

    # When msg is success background color is green, fail is red. So we need create two different variable for check
    # After return to templates. Template can easy css and understand
    if ins_new_price_msg_success is not None:
        return_para['ins_new_price_msg_success'] = ins_new_price_msg_success
    if ins_new_price_msg_fail is not None:
        return_para['ins_new_price_msg_fail'] = ins_new_price_msg_fail

    if get_price_database == False:
        return_para['err_msg'] = "Have some problem. Can't load all price"
    elif type(get_price_database['all_price']) is list:
        return_para['all_price'] = get_price_database['all_price']

    return render_template('price_editor_page.html', **return_para)


@editor_blueprint.route('/duration', methods = ['GET', 'POST'])
def duration_editor_page():
    if request.method == 'POST':
        if 'submit_ins_new_duration' in request.form:
            post_new_duration = request.form['new_duration']
            post_new_start_time = request.form['new_start_time']
            post_new_end_time = request.form['new_end_time']
            post_new_duration_name = request.form['new_name']

            # Because end time can accept two values. First is datetime like 2023-12-12 12:12:12
            # Second is empty string. if empty string it mean editor not input end time so end time is infinite
            if len(post_new_end_time) == 0:
                post_new_end_time = None

            # Insert new duration.
            exec_ins_database = exec_data.post_new_duration(new_duration_para = post_new_duration,
                new_start_time_para = post_new_start_time, new_end_time_para = post_new_end_time, new_name_para = post_new_duration_name)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.duration_editor_page', ins_new_duration_msg_success = 'Tạo giá mới thành công'))
            else:
                return redirect(url_for('EDITOR_bp.duration_editor_page', ins_new_duration_msg_fail = 'Tạo giá mới thất bại'))
    
    return_para = {}
    get_duration_database = exec_data.get_all_duration()

    ins_new_duration_msg_success = request.args.get('ins_new_duration_msg_success')
    ins_new_duration_msg_fail = request.args.get('ins_new_duration_msg_fail')
    
    # When msg is success background color is green, fail is red. So we need create two different variable for check
    # After return to templates. Template can easy css and understand
    if ins_new_duration_msg_success is not None:
        return_para['ins_new_duration_msg_success'] = ins_new_duration_msg_success
    if ins_new_duration_msg_fail is not None:
        return_para['ins_new_duration_msg_fail'] = ins_new_duration_msg_fail

    if get_duration_database == False:
        return_para['err_msg'] = "Have some problem. Can't load all duration"
    elif type(get_duration_database['all_duration']) is list:
        return_para['all_duration'] = get_duration_database['all_duration']

    return render_template('duration_editor_page.html', **return_para)

@editor_blueprint.route('/service', methods = ['GET', 'POST'])
def service_editor_page():
    return render_template('service_editor_page.html')

@editor_blueprint.route('/service/defaults', methods = ['GET', 'POST'])
def default_service_editor_page():
    if request.method == 'POST':
        if 'ins_new_default_service' in request.form:
            post_name_default_service = request.form['new_name_default_service']

            exec_ins_database = exec_data.post_new_default_service(new_name_para = post_name_default_service)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.default_service_editor_page', 
                    msg_ins_new_default_service_success = 'Thêm dịch vụ mặc định thành công'))
            else:
                return redirect(url_for('EDITOR_bp.default_service_editor_page', 
                    msg_ins_new_default_service_fail = 'Thêm dịch vụ mặc định thất bại'))
            
    return_para = {}

    msg_ins_new_default_service_success = request.args.get('msg_ins_new_default_service_success')
    msg_ins_new_default_service_fail = request.args.get('msg_ins_new_default_service_fail')

    if msg_ins_new_default_service_success is not None:
        return_para['msg_ins_new_default_service_success'] = msg_ins_new_default_service_success
    if msg_ins_new_default_service_fail is not None:
        return_para['msg_ins_new_default_service_fail'] = msg_ins_new_default_service_fail

    return render_template('default_service_editor_page.html', **return_para)

@editor_blueprint.route('/service/options', methods = ['GET', 'POST'])
def option_service_editor_page():
    if request.method == 'POST':
        if 'ins_new_option_service' in request.form:
            post_new_name = request.form['new_name_option_service']
            post_id_price = request.form['new_price_option_service']
            
            exec_ins_database = exec_data.post_new_option_service(new_name_para = post_new_name, new_id_para = post_id_price)

            if exec_ins_database == True:
                return redirect(url_for('EDITOR_bp.option_service_editor_page', 
                    msg_ins_new_option_service_success = 'Tạo dịch vụ tự chọn thành công'))
            else:
                return redirect(url_for('EDITOR_bp.option_service_editor_page', 
                    msg_ins_new_option_service_fail = 'Tạo dịch vụ tự chọn thất bại'))

    all_price = exec_data.get_all_price()
    return_para = {}

    if all_price != False:
        return_para['all_price'] = all_price['all_price']

    msg_ins_new_option_service_success = request.args.get('msg_ins_new_option_service_success')
    msg_ins_new_option_service_fail = request.args.get('msg_ins_new_option_service_fail')

    if msg_ins_new_option_service_success is not None:
        return_para['msg_ins_new_option_service_success'] = msg_ins_new_option_service_success
    if msg_ins_new_option_service_fail is not None:
        return_para['msg_ins_new_option_service_fail'] = msg_ins_new_option_service_fail

    return render_template('option_service_editor_page.html', **return_para)

@editor_blueprint.route('/service/show-all-service')
def show_all_service():
    all_service = exec_data.get_all_service()
    return_para = {}

    if all_service != False:
        return_para['all_default_service'] = all_service['all_default_service']
        return_para['all_option_service'] = all_service['all_option_service']

    return render_template('show_all_service.html', **return_para)

@editor_blueprint.route('/history-booking-room/<status_booking_room>/<int:current_page_para>')
def show_all_booking_room(status_booking_room, current_page_para = 1):
    return_para = {}

    get_booking_room = exec_data.get_booking_room(status_booking_para = status_booking_room, current_page_para = current_page_para)

    if get_booking_room != False:
        return_para['lst_booking_room'] = get_booking_room['lst_booking_room']
        return_para['lst_price'] = get_booking_room['lst_price']
        return_para['lst_all_room'] = get_booking_room['lst_all_room']
        return_para['lst_name_user'] = get_booking_room['lst_name_user']

    return_para['pagination_page'] = get_booking_room['pagination_page']
    return_para['current_page'] = current_page_para
    return_para['status_booking_room'] = status_booking_room

    return render_template('show_all_booking_room.html', **return_para)

@editor_blueprint.route('/history-booking-room/details/<code_booking_room>', methods = ['GET', 'POST'])
def detail_booking_room_by_code(code_booking_room):
    if request.method == 'POST':
        if 'confirm_cancel_booking' in request.form:
            post_cancel_booking_status = exec_data.post_status_booking_room_to_cancel(code_booking_room)
            if post_cancel_booking_status == True:
                return redirect(url_for('EDITOR_bp.detail_booking_room_by_code', code_booking_room = code_booking_room, msg_cancel_booking_status_success = 'True'))
            else:
                return redirect(url_for('EDITOR_bp.detail_booking_room_by_code', code_booking_room = code_booking_room, msg_cancel_booking_status_fail = 'Fail'))
        elif 'confirm_delete_booking' in request.form:
            pass
        elif 'confirm_edit_booking' in request.form:
            pass

    return_para = {}

    msg_cancel_booking_status_success = request.args.get('msg_cancel_booking_status_success')
    msg_cancel_booking_status_fail = request.args.get('msg_cancel_booking_status_fail')

    if msg_cancel_booking_status_success is not None:
        return_para['msg_cancel_booking_status_success'] = 'Hủy thành công đơn hàng'
    if msg_cancel_booking_status_fail is not None:
        return_para['msg_cancel_booking_status_fail'] = 'Hủy đơn hàng thất bại'
        
    get_booking_room_by_code = exec_data.get_booking_room_by_code(code_booking_room)

    if get_booking_room_by_code != False:
        return_para['get_booking_room_by_code'] = get_booking_room_by_code['get_booking_room_by_code']
        return_para['get_room_by_code'] = get_booking_room_by_code['get_room_by_code']
        return_para['get_price_room'] = get_booking_room_by_code['get_price_room']
        return_para['get_img_name_firebase_room'] = get_booking_room_by_code['get_img_name_firebase_room']

        return_para['get_default_service_room'] = ast.literal_eval(get_booking_room_by_code['get_room_by_code'].default_service)
        return_para['get_option_service_room'] = ast.literal_eval(get_booking_room_by_code['get_room_by_code'].option_service)

        return_para['get_default_service_booking'] = get_booking_room_by_code['get_booking_room_by_code'].default_service
        return_para['get_option_service_booking'] = get_booking_room_by_code['get_booking_room_by_code'].option_service

    return render_template('detail_booking_room_by_code.html', **return_para)