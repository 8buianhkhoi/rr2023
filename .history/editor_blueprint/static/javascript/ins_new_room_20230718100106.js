var dict_amount_default_service = {}

function add_default_service(){
    let choose_default_service = document.getElementsByClassName('choose__default__service')[0]
    let value_choose_default_service = choose_default_service.value 
    let option_choose_default_service = choose_default_service.querySelector('option[value="' + value_choose_default_service + '"]')
    let text_choose_default_service = option_choose_default_service.textContent
    
    if (text_choose_default_service in dict_amount_default_service){
        dict_amount_default_service[text_choose_default_service] += 1
    }
    else {
        dict_amount_default_service[text_choose_default_service] = 1
    }

    console.log(dict_amount_default_service)
    let new_div_tag = document.createElement('div')
    let new_btn_tag = document.createElement('button')
    let new_li_tag = document.createElement('li')
    let new_div_amount_tag = document.createElement('div')
    let new_plus_btn_amount = document.createElement('button')
    let new_minus_btn_amount = document.createElement('button')
    let new_amount_text = document.createElement('p')

    let text_li_tag = document.createTextNode(text_choose_default_service)
    let text_p_tag = document.createTextNode('x')
    let text_minus_amount_btn = document.createTextNode('-')
    let text_plus_amount_btn = document.createTextNode('+')

    new_li_tag.appendChild(text_li_tag)
    new_btn_tag.appendChild(text_p_tag)
    new_minus_btn_amount.appendChild(text_minus_amount_btn)
    new_plus_btn_amount.appendChild(text_plus_amount_btn)
    
    new_div_amount_tag.append(new_minus_btn_amount)
    new_div_amount_tag.append(new_amount_text)
    new_div_amount_tag.append(new_plus_btn_amount)

    new_div_amount_tag.classList.add('show__amount__each__default__service')

    new_div_tag.append(new_li_tag)
    new_div_tag.append(new_div_amount_tag)
    new_div_tag.append(new_btn_tag)

    document.getElementsByClassName('lst__add__default__service')[0].appendChild(new_div_tag)
}