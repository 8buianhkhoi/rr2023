var dict_amount_default_service = {}
var dict_amount_option_service = {}
var dict_all_image_new_room = {}
var name_option_service;
var name_default_service;

function show_option_service(){
    document.getElementsByClassName('lst__add__option__service')[0].innerHTML = ""
    document.getElementsByClassName('lst__option__service__area')[0].innerText = JSON.stringify(dict_amount_option_service)

    for ( let index in dict_amount_option_service){
        let amount_temp = dict_amount_option_service[index]

        let new_div_tag = document.createElement('div')
        let new_btn_tag = document.createElement('button')
        let new_li_tag = document.createElement('li')
        let new_div_amount_tag = document.createElement('div')
        let new_plus_btn_amount = document.createElement('button')
        let new_minus_btn_amount = document.createElement('button')
        let new_amount_text = document.createElement('p')

        new_plus_btn_amount.setAttribute('onclick', `plus_each_amount_option_service("${index}")`)
        new_minus_btn_amount.setAttribute('onclick', `minus_each_amount_option_service("${index}")`)
        new_btn_tag.setAttribute('onclick', `del_each_option_service("${index}")`)
        new_btn_tag.setAttribute('type', 'button')

        let text_li_tag = document.createTextNode(index)
        let text_p_tag = document.createTextNode('x')
        let text_minus_amount_btn = document.createTextNode('-')
        let text_plus_amount_btn = document.createTextNode('+')
        let text_amount_p = document.createTextNode(amount_temp)

        new_li_tag.appendChild(text_li_tag)
        new_btn_tag.appendChild(text_p_tag)
        new_minus_btn_amount.appendChild(text_minus_amount_btn)
        new_plus_btn_amount.appendChild(text_plus_amount_btn)
        new_amount_text.appendChild(text_amount_p)
        
        new_div_amount_tag.append(new_minus_btn_amount)
        new_div_amount_tag.append(new_amount_text)
        new_div_amount_tag.append(new_plus_btn_amount)

        new_div_amount_tag.classList.add('show__amount__each__option__service')

        new_div_tag.append(new_li_tag)
        new_div_tag.append(new_div_amount_tag)
        new_div_tag.append(new_btn_tag)

        document.getElementsByClassName('lst__add__option__service')[0].appendChild(new_div_tag)
    }
}

function show_default_service(){
    document.getElementsByClassName('lst__add__default__service')[0].innerHTML = ""
    document.getElementsByClassName('lst__default__service__area')[0].innerText = JSON.stringify(dict_amount_default_service)

    for ( let index in dict_amount_default_service){
        let amount_temp = dict_amount_default_service[index]

        let new_div_tag = document.createElement('div')
        let new_btn_tag = document.createElement('button')
        let new_li_tag = document.createElement('li')
        let new_div_amount_tag = document.createElement('div')
        let new_plus_btn_amount = document.createElement('button')
        let new_minus_btn_amount = document.createElement('button')
        let new_amount_text = document.createElement('p')

        new_plus_btn_amount.setAttribute('onclick', `plus_each_amount_default_service("${index}")`)
        new_minus_btn_amount.setAttribute('onclick', `minus_each_amount_default_service("${index}")`)
        new_btn_tag.setAttribute('onclick', `del_each_default_service("${index}")`)
        new_btn_tag.setAttribute('type','button')

        let text_li_tag = document.createTextNode(index)
        let text_p_tag = document.createTextNode('x')
        let text_minus_amount_btn = document.createTextNode('-')
        let text_plus_amount_btn = document.createTextNode('+')
        let text_amount_p = document.createTextNode(amount_temp)

        new_li_tag.appendChild(text_li_tag)
        new_btn_tag.appendChild(text_p_tag)
        new_minus_btn_amount.appendChild(text_minus_amount_btn)
        new_plus_btn_amount.appendChild(text_plus_amount_btn)
        new_amount_text.appendChild(text_amount_p)
        
        new_div_amount_tag.append(new_minus_btn_amount)
        new_div_amount_tag.append(new_amount_text)
        new_div_amount_tag.append(new_plus_btn_amount)

        new_div_amount_tag.classList.add('show__amount__each__default__service')

        new_div_tag.append(new_li_tag)
        new_div_tag.append(new_div_amount_tag)
        new_div_tag.append(new_btn_tag)

        document.getElementsByClassName('lst__add__default__service')[0].appendChild(new_div_tag)
    }
}

function add_option_service(){
    let choose_option_service = document.getElementsByClassName('choose__option__service')[0]
    let value_choose_option_service = choose_option_service.value 
    let option_choose_option_service = choose_option_service.querySelector('option[value="' + value_choose_option_service + '"]')
    let text_choose_option_service = option_choose_option_service.textContent
    

    if (text_choose_option_service in dict_amount_option_service){
        dict_amount_option_service[text_choose_option_service] += 1
    }
    else {
        dict_amount_option_service[text_choose_option_service] = 1
    }

    show_option_service()

}

function add_default_service(){
    let name_default_service = document.getElementsByClassName('name__default__service__text')[0].value
    let quantity_default_service = document.getElementsByClassName('quantity__default__service__text')[0].value 

    if (name_default_service in dict_amount_default_service){
        dict_amount_default_service[name_default_service] += parseInt(quantity_default_service)
    }
    else {
        dict_amount_default_service[name_default_service] = parseInt(quantity_default_service)
    }

    show_default_service()
    
}

function minus_each_amount_default_service(name_default_service){
    dict_amount_default_service[name_default_service] -= 1

    if (dict_amount_default_service[name_default_service] === 0 ){
        if(confirm('Bạn có muốn xóa dịch vụ mặc định này ?')){
            delete dict_amount_default_service[name_default_service]
            show_default_service()
        }
        else{
            dict_amount_default_service[name_default_service] = 1
            show_default_service()
        }
    }
    else {
        show_default_service()
    }
    
}

function plus_each_amount_default_service(name_default_service){
    dict_amount_default_service[name_default_service] += 1
    show_default_service()
}


function plus_each_amount_option_service(name_option_service){
    dict_amount_option_service[name_option_service] += 1
    show_option_service()
}

function minus_each_amount_option_service(name_option_service){
    dict_amount_option_service[name_option_service] -= 1

    if (dict_amount_option_service[name_option_service] === 0 ){
        if(confirm('Bạn có muốn xóa dịch vụ mặc định này ?')){
            delete dict_amount_option_service[name_option_service]
            show_option_service()
        }
        else{
            dict_amount_option_service[name_option_service] = 1
            show_option_service()
        }
    }
    else {
        show_option_service()
    }
}

function del_each_option_service(name_option_service_para){
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(2px)'
    document.getElementsByClassName('confirm__delete__option__service')[0].style.display = 'block'
    name_option_service = name_option_service_para;
}

function del_each_default_service(name_default_service_para){
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(2px)'
    document.getElementsByClassName('confirm__delete__default__service')[0].style.display = 'block'
    name_default_service = name_default_service_para
}

function accept_confirm_del_option_service(){
    delete dict_amount_option_service[name_option_service]
    name_option_service = ''
    document.getElementsByClassName('confirm__delete__option__service')[0].style.display = 'none'
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(0px)'
    show_option_service()
}

function accept_confirm_del_default_service(){
    delete dict_amount_default_service[name_default_service]
    name_default_service = ''
    document.getElementsByClassName('confirm__delete__default__service')[0].style.display = 'none'
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(0px)'
    show_default_service()
}

function cancel_confirm_del_option_service(){
    name_option_service = ''
    document.getElementsByClassName('confirm__delete__option__service')[0].style.display = 'none'
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(0px)'
}

function cancel_confirm_del_default_service(){
    name_default_service = ''
    document.getElementsByClassName('confirm__delete__default__service')[0].style.display = 'none'
    document.getElementsByClassName('form__ins__inform__room')[0].style.filter = 'blur(0px)'
}

function get_image_new_room(event){
    let fileImage = event.target.files[0]

    let reader = new FileReader();

    reader.onload = function(event){
        let new_img_tag = document.createElement('img')
        let new_div_tag = document.createElement('div')
        let new_btn_tag = document.createElement('button')
        let new_text_btn_tag = document.createTextNode('X')

        new_btn_tag.appendChild(new_text_btn_tag)
        new_div_tag.append(new_img_tag)
        new_div_tag.append(new_btn_tag)

        new_btn_tag.setAttribute('onclick', 'remove_image_new_room(this)')

        new_img_tag.setAttribute('src', event.target.result)

        dict_all_image_new_room[String(Object.keys(dict_all_image_new_room).length + 1)] = event.target.result

        document.getElementsByClassName('lst__all__image__new__room')[0].innerText = JSON.stringify(dict_all_image_new_room)
        document.getElementsByClassName('show__all__image__new__room__inner')[0].appendChild(new_div_tag)
    }

    reader.readAsDataURL(fileImage);
}

function loadDistrict(){
    let province = document.getElementsByClassName('province__select__new__room')[0].value

    $.ajax({
        url:'/get-address/get-district',
        method: 'POST',
        data: {province:province},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementsByClassName('district__select__new__room')[0]
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}


function loadWard(){
    let district = document.getElementsByClassName('district__select__new__room')[0].value
    let province = document.getElementsByClassName('province__select__new__room')[0].value

    $.ajax({
        url:'/get-address/get-wards',
        method: 'POST',
        data: {province:province, district :district},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementsByClassName('ward__select__new__room')[0]
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}


function onLoadProvince(){
    $.ajax({
        url:'/get-address/get-province',
        method: 'POST',
        data: {},
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('province__select__new__room');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function remove_image_new_room(img_remove){
    img_remove.parentElement.remove()
}