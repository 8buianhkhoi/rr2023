function loadDistrict(){
    let province = document.getElementsByClassName('province__select__edit__room')[0].value

    $.ajax({
        url:'/get-address/get-district',
        method: 'POST',
        data: {province:province},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementsByClassName('district__select__edit__room')[0]
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}


function loadWard(){
    let district = document.getElementsByClassName('district__select__edit__room')[0].value
    let province = document.getElementsByClassName('province__select__edit__room')[0].value

    $.ajax({
        url:'/get-address/get-wards',
        method: 'POST',
        data: {province:province, district :district},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementsByClassName('ward__select__edit__room')[0]
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}


function onLoadProvince(){
    $.ajax({
        url:'/get-address/get-province',
        method: 'POST',
        data: {},
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('province__select__edit__room');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function