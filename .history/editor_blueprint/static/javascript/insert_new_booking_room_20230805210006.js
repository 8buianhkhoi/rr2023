var default_service_add = {}
var option_service_add = {}

function btn_add_default_service(name_default_service_para){
    if (name_default_service_para in default_service_add) {
        let maximum_amount_default_service = get_default_service_by_code_room[name_default_service_para]

        if (default_service_add[name_default_service_para] < maximum_amount_default_service){
            default_service_add[name_default_service_para] += 1
        }
        else {
            alert('Bạn thêm vượt quá giới hạn')
        }
    }
    else{
        default_service_add[name_default_service_para] = 1
    }
    load_default_service_add()
}

function btn_add_option_service(name_option_service_para){
    if (name_option_service_para in option_service_add){
        let maximum_amount_option_service = dict_price_option_service[name_option_service_para]['amount']

        if (option_service_add[name_option_service_para]['amount'] < maximum_amount_option_service){
            option_service_add[name_option_service_para]['amount'] += 1
            option_service_add[name_option_service_para]['price'] = option_service_add[name_option_service_para]['price'] + dict_price_option_service[name_option_service_para]['price']
        }
        else{
            alert('Bạn thêm vượt quá giới hạn')
        }
    }
    else{
        option_service_add[name_option_service_para] = {'amount' : 1, 'price' : dict_price_option_service[name_option_service_para]['price']}
    }
}

function load_default_service_add(){
    document.getElementsByClassName('all__row__default__service')[0].innerHTML = ''
    let index_row_default_service = 1

    let new_caption_tag = document.createElement('caption')
    new_caption_tag.textContent = 'Default service add to booking'
    
    let new_tr_header = document.createElement('tr')
    let new_th_1_header = document.createElement('th')
    let new_th_2_header = document.createElement('th')
    let new_th_3_header = document.createElement('th')

    for (let each_default_service in default_service_add){
        let new_row_default_service = document.createElement('tr')

        let new_col_1 = document.createElement('td')
        new_col_1.textContent = String(index_row_default_service)

        let new_col_2 = document.createElement('td')
        new_col_2.textContent = each_default_service

        let new_col_3 = document.createElement('td')
        new_col_3.textContent = default_service_add[each_default_service]

        new_row_default_service.append(new_col_1)
        new_row_default_service.append(new_col_2)
        new_row_default_service.append(new_col_3)

        document.getElementsByClassName('all__row__default__service')[0].append(new_row_default_service)
    }
}