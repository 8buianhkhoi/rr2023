var default_service_add = {}
var option_service_add = {}

function btn_add_default_service(name_default_service_para){
    if (name_default_service_para in default_service_add) {
        let maximum_amount_default_service = get_default_service_by_code_room[name_default_service_para]

        if (default_service_add[name_default_service_para] < maximum_amount_default_service){
            default_service_add[name_default_service_para] += 1
        }
        else {
            alert('Bạn thêm vượt quá giới hạn')
        }
    }
    else{
        default_service_add[name_default_service_para] = 1
    }
    load_default_service_add()
}

function btn_add_option_service(name_option_service_para){
    if (name_option_service_para in option_service_add){
        let maximum_amount_option_service = dict_price_option_service[name_option_service_para]['amount']

        if (option_service_add[name_option_service_para]['amount'] < maximum_amount_option_service){
            option_service_add[name_option_service_para]['amount'] += 1
            option_service_add[name_option_service_para]['price'] = option_service_add[name_option_service_para]['price'] + dict_price_option_service[name_option_service_para]['price']
        }
        else{
            alert('Bạn thêm vượt quá giới hạn')
        }
    }
    else{
        option_service_add[name_option_service_para] = {'amount' : 1, 'price' : dict_price_option_service[name_option_service_para]['price']}
    }
}

function load_default_service_add(){
    document.getElementsByClassName('all__row__default__service')[0].innerHTML = ''
    let index_row_default_service = 1
    
    for (let each_default_service in default_service_add){
        let new_row_default_service = document.createElement('tr')

        let new_col
    }
}