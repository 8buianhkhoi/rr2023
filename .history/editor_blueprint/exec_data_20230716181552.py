from sqlalchemy import select

from models.models import *

def get_all_price():
    try:
        with engine.connect() as conn:
            query_select = select(price)
            all_price = conn.execute(query_select).fetchall()
        return {'all_price' : all_price}
    except:
        return False

def get_all_duration():
    try:
        with engine.connect() as conn:
            query_select = select(duration)
            all_duration = conn.execute(query_select).fetchall()
        return {'all_product' : all_duration}
