from flask import Blueprint, render_template

editor_blueprint = Blueprint('EDITOR_bp', __name__, static_folder = 'static', template_folder = 'templates')

@editor_blueprint.route('/')
def homepage_editor_page():
    return render_template('homepage_editor_page.html')