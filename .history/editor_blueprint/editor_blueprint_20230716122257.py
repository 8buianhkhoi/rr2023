from flask import Blueprint, render_template, session, redirect, url_for

from . import exec_data

editor_blueprint = Blueprint('EDITOR_bp', __name__, static_folder = 'static', template_folder = 'templates')

@editor_blueprint.route('/')
def homepage_editor_page():
    return render_template('homepage_editor_page.html')

@editor_blueprint.route('/insert-new-room')
def ins_new_room():
    return render_template('ins_new_room.html')

@editor_blueprint.route('/log-out')
def log_out():
    session.clear()
    return redirect(url_for('HP_bp.homepage_page'))

@editor_blueprint.route('/price-and-duration')
def price_and_duration():
    return render_template('price_and_duration.html')

@editor_blueprint.route('/price')
def price_editor_page():
    get_price_database = exec_data.get_all_price()

    if get_price_database == False:
        return render_template('price_editor_page.html', err_msg = 'Have some problem. Not load all price')
    elif type(get_price_database['all_price']) is list:
        return render_template('price_editor_page.html', all_price = get_price_database['all_price'])

    return render_template('price_editor_page.html')

@editor_blueprint.route('duration')
def duration_editor_page():
    return render_template('duration_editor_page.html')