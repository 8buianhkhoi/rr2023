from flask import Blueprint

generate_pdf_blueprint = Blueprint('GEN_PDF_bp', __name__, static_folder = 'static', template_folder = 'template')