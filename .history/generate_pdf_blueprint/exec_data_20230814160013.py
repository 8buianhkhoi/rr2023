from sqlalchemy import select, and_
import platform

from models.models import *
from . import get_pagination

limit_show_per_page = 30

# This function check OS. Linux os need to do something more than Windows and Darwin ( MacOS )
# In this function if linux os, need to add commit function for execute database ( insert, delete, update)
def check_current_os(conn):
    if platform.system() == 'Linux':
        conn.commit()
    elif platform.system() == 'Darwin':
        pass 
    elif platform.system() == 'Windows':
        pass

def get_all_booking_room(current_page_para):
    try:
        with engine.connect() as conn:
            query_get_all_booking = select(booking_room)
            length_all_booking = conn.execute(query_get_all_booking).rowcount

            get_pagination_page = get_pagination.get_pagination(current_page_para, limit_show_per_page, length_all_booking)

            get_all_booking = conn.execute(query_get_all_booking.offset(get_pagination_page[1]).limit(limit_show_per_page)).fetchall()
        
        return {'get_all_booking' : get_all_booking, 'pagination_page' : get_pagination_page[0]}
    except:
        return False