# Import library
from models.models import *
import datetime

# Note : This file not public in the server or git hub. This file just use in local.
# This file help create demo database for testing. Please do not edit or delete this file

def ins_new_price_editor_page():
    # value in lst_price is random, not follow any rule, this price
    lst_price = ["30000", "20000", "1000", "5000000", "10000", "250000", "2000500", "150000", "1580000", "2500000"]
    with engine.connect() as conn:
        for index in range(1,11):
            start_time = datetime.datetime.now().strftime(f'%d-%m-%Y %H:%M:%S')
            end_time = (datetime.datetime.now() + datetime.timedelta(days=2)).strftime(f'%d-%m-%Y %H:%M:%S')