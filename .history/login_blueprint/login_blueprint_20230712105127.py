from flask import Blueprint, render_template

login_blueprint = Blueprint('Login_bp', __name__, static_folder = 'static', template_folder = 'templates')