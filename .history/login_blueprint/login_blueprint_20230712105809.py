from flask import Blueprint, render_template

login_blueprint = Blueprint('Login_bp', __name__, static_folder = 'static', template_folder = 'templates')

@login_blueprint.route('/login-account')
def login_account():
    return render_template('login_account.html')