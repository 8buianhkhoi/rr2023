from flask import Blueprint, render_template, request, session, redirect, url_for
from dotenv import load_dotenv
import hashlib
import base64
import os
import jwt
import datetime

from . import exec_data

login_blueprint = Blueprint('Login_bp', __name__, static_folder = 'static', template_folder = 'templates')

def hash_pass(password, salt = None, iterations = 100000, key_length = 32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen = key_length)
    return salt + key 

@login_blueprint.route('/', methods=['GET','POST'])
def login_account():
    if request.method == 'POST':
        get_username_login = request.form['username_login']
        get_password_login = request.form['password_login']

        load_dotenv()
        secret_key = os.getenv('secret_key')

        hash_bytes = hash_pass(get_password_login, secret_key.encode())
        get_password_login = base64.b64encode(hash_bytes).decode('utf-8')

        check_login = exec_data.check_login(username_login = get_username_login, password_login = get_password_login)
        
        if check_login == []:
            return render_template('login_account.html', msg = "Tên đăng nhập hoặc mật khẩu không đúng")
        else:
            start_time_token = datetime.datetime.now().strftime(f'%d-%m-%Y %H:%M:%S')
            end_time_token = (datetime.datetime.now() + datetime.timedelta(days = 2)).strftime(f'%d-%m-%Y %H:%M:%S')
            dict_token = {'full_name' : check_login[0][0], 'username' : check_login[0][1], 'id_login' : check_login[0][3],
                'start_time_token' : start_time_token, 'end_time_token' : end_time_token}

            session['token_rr_2023'] = jwt.encode(dict_token, secret_key, algorithm = 'HS256')

            role_user = check_login[0][2]
            if role_user == 'user':
                return redirect(url_for('USER_bp.type_room_homepage', type_room = 'all'))
            elif role_user == 'editor':
                return redirect(url_for('EDITOR_bp.homepage_editor_page'))

    return render_template('login_account.html')