from flask import Blueprint, render_template
from dotenv import load_dotenv
import hashlib
import base64
import os

from . import exec_data
login_blueprint = Blueprint('Login_bp', __name__, static_folder = 'static', template_folder = 'templates')

@login_blueprint.route('/')
def login_account():
    return render_template('login_account.html')