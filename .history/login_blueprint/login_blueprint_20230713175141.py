from flask import Blueprint, render_template
from dotenv import load_dotenv
import hashlib
import base64
import os

from . import exec_data

login_blueprint = Blueprint('Login_bp', __name__, static_folder = 'static', template_folder = 'templates')

@login_blueprint.route('/', methods=['GET','POST'])
def login_account():
    if request.method == 'POST':
        username_login = request.form['username_login']
        password_login = request.form['password_login']

        load_dotenv()
        secret_key = os.getenv('secret_key')

        hash_bytes = hash_pass(password_login, secret_key.encode())
        password_login = base64.b64encode(hash_bytes).decode('utf-8')

    return render_template('login_account.html')