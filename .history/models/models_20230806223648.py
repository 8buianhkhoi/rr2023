# Import library
# Using sqlalchemy library to connect database and execute database
# Using platform library to check OS ( linux, windows, darwin )
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey, DateTime, Time
from sqlalchemy.dialects.mysql import JSON
import platform

# Create engine
# Name schema in mysql is rr2023

if platform.system() == 'Linux':
    engine = create_engine("mysql+pymysql://uservantai:CncDB22_31@127.0.0.1/meetingrooms")
elif platform.system() == 'Windows':
    engine = create_engine("mysql+pymysql://root:0338592545A#@localhost/rr2023")

meta = MetaData()

users = Table(
    'users', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('user_name', String(45), nullable = False, unique = True),
    Column('password', String(1000), nullable = False),
    Column('full_name', String(45), nullable = False, unique = True),
    Column('tel', String(25), nullable = False, unique = True),
    Column('gmail', String(45), nullable = False, unique = True),
    Column('note', String(1000), nullable = True),
    Column('status', String(25), nullable = False),
    Column('province', String(255), nullable = True),
    Column('district', String(255), nullable = True),
    Column('ward', String(255), nullable = True),
    Column('gender', String(25), nullable = False),
    Column('type_user', String(25), nullable = False)
)

# capacity : how many people in room, capacity not effect price column
# type : accept 3 values are : small, medium, big
# duration : accept range of time, base seconds, like 2 hour is 7200 second
all_room = Table(
    'all_room', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('code_room', String(255), nullable = False),
    Column('name_room', String(255), nullable = False),
    Column('capacity', String(45), nullable = False),
    Column('square', String(45), nullable = False),
    Column('type', String(45), nullable = False),
    Column('province', String(255), nullable = False),
    Column('district', String(255), nullable = False),
    Column('ward', String(255), nullable = False),
    Column('id_duration', Integer, ForeignKey('duration.id'), nullable = False),
    Column('id_price', Integer, ForeignKey('price.id'), nullable = False),
    Column('default_service', JSON, nullable = True),
    Column('option_service', JSON, nullable = True),
    Column('name_img_firebase', JSON, nullable = False)
)

# service have two type : default and option
# if type default price = 0
default_service_room = Table(
    'default_service_room', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('name', String(255), nullable = False)
)

option_service_room =  Table(
    'option_service_room', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('name', String(255), nullable = False),
    Column('id_price', Integer, ForeignKey('price.id'), nullable = False)
)

# Status accept 2 values : pending and finish. Pending : room for rent ok, but not rent room finish. Finish : rent room finish
booking_room = Table(
    'booking_room', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('id_users', Integer, ForeignKey('users.id'), nullable = False),
    Column('id_all_room', Integer, ForeignKey('all_room.id'), nullable = False),
    Column('default_service', JSON, nullable = True),
    Column('option_service', JSON, nullable = True),
    Column('time_booking', DateTime, nullable = False),
    Column('time_start', DateTime, nullable = False),
    Column('minute_rent', String(255), nullable = False),
    Column('status', String(25), nullable = False),
    Column('option_service_price', String(255), nullable = False),
    Column('total_price', String(255), nullable = False),
    Column('note_booking', String(1000), nullable = True),
    Column('code_booking', String(255), nullable = False),
    Column('billing_option', String(255), nullable = False),
    Column('flag_booking', String(255), nullable = False)
)

price = Table(
    'price', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('price', String(255), nullable = False),
    Column('date_start_price', DateTime, nullable = False),
    Column('date_end_price', DateTime, nullable = True),
    Column('full_name', String(45), nullable = False)
)

duration = Table(
    'duration', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('duration', String(255), nullable = False),
    Column('date_start_duration', DateTime, nullable = False),
    Column('date_end_duration', DateTime, nullable = True),
    Column('full_name', String(45), nullable = False)
)

division = Table(
    'division', meta,
    Column('idDivision', Integer, primary_key = True, autoincrement = True),
    Column('province', String(255), nullable = True),
    Column('district', String(255), nullable = True),
    Column('ward', String(255), nullable = True),
    Column('latitude', String(255), nullable = True),
    Column('longitude', String(255), nullable = True)
)

payment_method = Table(
    'payment_method', meta,
    Column('id', Integer, primary_key = True, autoincrement = True)
)

meta.create_all(engine)