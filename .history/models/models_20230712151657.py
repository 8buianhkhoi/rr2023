# Import library
# Using sqlalchemy library to connect database and execute database
# Using platform library to check OS ( linux, windows, darwin )
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey, DateTime, Time
import platform

# Create engine
# Name schema in mysql is rr2023
engine = create_engine("mysql+pymysql://root:0338592545A#@localhost/rr2023")

meta = MetaData()

users = Table(
    'users', meta,
    Column('id', Integer, primary_key = True, autoincrement = True),
    Column('user_name', String(45), nullable = False, unique = True),
    Column('password', String(45), nullable = False),
    Column('tel', String(25), nullable = False, unique = True),
    Column('gmail', String(45), nullable = False, unique = True),
    Column('note', String(1000), nullable = True),
    Column('status', String(25), nullable = False),
    Column('province', String(255), nullable = True),
    Column('district', String(255), nullable = True),
    Column('ward', String(255), nullable = True),
    Column('gender', String(25), nullable = False)
)

meta.create_all(engine)